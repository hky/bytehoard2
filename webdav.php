<?php

/*
 * ByteHoard 2.1
 * Copyright (c) Andrew Godwin & contributors 2004
 *
 *   webdav.php
 *   $Id$
 *
 */


# WebDAV Server file
define("IN_BH", 1);

# Get includes
require "includes/configfunc.inc.php";				# Config functions
bh_checkconfig();						# Check the config exists, or don't bother with the rest.
require "config.inc.php";					# Load config file
require "includes/db/".$dbconfig['dbmod'];			# Database functions (TODO: make user-selectable)
bh_loadconfig();						# Load configuration
require "includes/auth/".$bhconfig['authmodule'];		# Authentication functions 
require "langs/".$bhconfig['lang'].".lang.php";					# Language File 
require "includes/filesystem/".$bhconfig['filesystemmodule']."/filesystem.inc.php";	# Filesystem functions 
require "includes/filesystem/".$bhconfig['filesystemmodule']."/mimetype.inc.php";	# Mimetype functions
require "includes/filesystem/".$bhconfig['filesystemmodule']."/thumbnails.inc.php";	# Thumbnail functions 
require "layouts/".$bhconfig['layout']."/main.inc.php";		# Layout file 
require "includes/log.inc.php";					# Logging functions
require "includes/users.inc.php";				# User functions
require "includes/modules.inc.php";				# Module functions
require "includes/detect.inc.php";				# Detection functions
require "includes/bandwidth.inc.php";				# Bandwidth functions


require "includes/webdav/server.php";

class webdav extends HTTP_WebDAV_Server {

	# Constructor - Initialises settings, etc.
	function webdav() {
		global $bhconfig;
	
		parent::HTTP_WebDAV_Server();
		$this->http_auth_realm = $bhconfig['sitename'];
	}
	
	function check_auth($type, $username, $password) {
		global $bhconfig, $bhcurrent, $bhsession;
		
		$bhcurrent['in_webdav'] = 1;
		
		if (bh_authenticate($username, $password) == false) {
			$bhcurrent['username'] = "guest";
			$bhsession['username'] = "guest";
			$bhcurrent['usertype'] = "guest";
			return false;
		} else {
			$bhcurrent['username'] = $username;
			$bhsession['username'] = $username;
			$bhcurrent['userobj'] = new bhuser($username);
			$bhcurrent['usertype'] = $bhcurrent['userobj']->type;
			return true;
		}
	}
	
	function GET(&$options) {

		#a("_get_");
		#a($options['path']);

		global $bhsession;
	
		$fileexist = bh_user_file_exists($options['path']);
		
		if (!$fileexist) {
			return "404 Not found";
		}
	
		$fileobj = new bhfile($options['path']);
		$options['path'] = $options['path'];
		if ($fileobj->is_dir() == 1) {
			$options['mimetype'] = "directory";
		} else {
			$options['mimetype'] = $fileobj->mimetype();
		}
		$options['mtime'] = $fileobj->fileinfo['createdate'];
		$options['size'] = $fileobj->fileinfo['filesize'];
		$options['stream'] = $fileobj->filestream();
		
		
		return true;
	}
	
	function PUT(&$options) {
		global $bhsession;
	
		#$fn = fopen("wdlog", "w");
		#fwrite($fn, print_r($options, true));
		#fclose($fn);

		#a("_put_");
		#a($options['path']);

		$newfilepath = bh_fpclean($options['path']);
		

		if (bh_checkrights(bh_get_parent($newfilepath), $bhsession['username']) <= 1) {
			return "403 Forbidden";
		}

		$newfileobj = new bhfile($newfilepath);
		$infolder = bh_get_parent($filepath);			
		$newfiledirobj = new bhfile($infolder);

		if ( $newfiledirobj->is_dir() == false ) {
		            #a("Conflict");
			    #a(dirname($newfilepath));
		            return "409 Conflict";
		}

		$fileexist = bh_user_file_exists($newfilepath);
		if (!$fileexist) {
		        #a("New");
		        #a($newfilepath);
			$options["new"] = true;
		}

		$newfileobj->filefromstream($options['stream']);
		return '201 Created';
		
	}
	
	function DELETE(&$options) {
		global $bhsession;
		
		#a("_delete_");
		#a($options['path']);

		$fileexist = bh_user_file_exists($options['path']);
		if (!$fileexist) {
			return "404 Not found";
		}
		
		$fileobj = new bhfile($options['path']);
		$fileobj->smartdeletefile();
		
		return "204 No Content";
	}
	
	function MKCOL(&$options) {
		global $bhsession;

		#a("_mkcol_");
		#a($options['path']);
		
		$filepath = bh_fpclean($options['path']);
		$infolder = bh_get_parent($filepath);


		$parentexist = bh_user_file_exists($infolder);
		if (!$parentexist) {
			return "409 Conflict";
		}
		
		$newfiledirobj = new bhfile($infolder);
		if ( $newfiledirobj->is_dir() == false ) {
                        return "403 Forbidden";
		}


		$fileexist = bh_user_file_exists($filepath);
		if ($fileexist) {
			return "405 Method not allowed";
		}
		

	        if (!empty($this->_SERVER["CONTENT_LENGTH"])) { // no body parsing yet
        		return "415 Unsupported media type";
        	}


		
		if (bh_checkrights(bh_fpclean($infolder), $bhsession['username']) >= 2) {
			bh_mkdir($filepath);
			$fileobj = new bhfile($filepath);
			unset($fileobj);
			
			return "201 Created";
		} else {
			return "403 Forbidden";
		}
	}
	

	function MOVE(&$options) {

		#a("_move_");
		#a($options['path']);

	        return $this->COPY($options, true);
    	}
	
	function COPY(&$options, $move=false ) {
		global $bhsession;
	
		#a("_copy_");
		#a($options['path']);
		#a($options['dest']);


	        if (!empty($this->_SERVER["CONTENT_LENGTH"])) { // no body parsing yet
        		return "415 Unsupported media type";
        	}

	        // no copying to different WebDAV Servers yet
        	if (isset($options["dest_url"])) {
            		return "502 bad gateway";
        	}
		
	        $existing_col = false;

		$filepath = bh_fpclean($options['path']);
		$destfilepath = bh_fpclean($options['dest']);
		$infolder = bh_get_parent($destfilepath);

		$fileexist = bh_user_file_exists($filepath);
		$destfileexist = bh_user_file_exists($destfilepath);

		# source do not exist
		if (!$fileexist) { return "404 Not Found"; }
		
		$fileobj        = new bhfile($filepath);
		$destfileobj    = new bhfile($destfilepath);
		$is_dir_file    = $fileobj->is_dir();
		$is_dir_dest    = $destfileobj->is_dir();

		#a("filepath: ".$filepath);
		#a("destfilepath: ".$destfilepath);
		#a("isdirsource: ".$is_dir_file);
		#a("isdirdest: ".$is_dir_dest);


	        if ($destfileexist) {
        	    if ($move && is_dir_dest) {
                	if (!$options["overwrite"]) {
                	    	return "412 precondition failed";
                	}

        	        $destfilepath .= basename($filepath);
                	if (bh_user_file_exists($destfilepath)) {
                    		$options["dest"] .= basename($filepath);
                    		$destfileexist = true;
                	} else {
                    		$destfileexist = false;
                    		$existing_col = true;
	                }
        	    }
        	}

	        if ($destfileexist) {
        	    if ($options["overwrite"]) {
                	$stat = $this->DELETE(array("path" => $options["dest"]));
                	if (($stat{0} != "2") && (substr($stat, 0, 3) != "404")) {
	                    	return $stat; 
                	}
            	    } else {
                	return "412 precondition failed";
            	    }
        	}


		# test if the target dir do not exist
		$infolderexist = bh_user_file_exists($infolder);
		if (!$infolderexist) {
			return "409 Conflict";
		}


####### This fail on test but in my opinion is it right (depth=0 not supported) !!!

        if (is_dir_file && ($options["depth"] != "infinity")) {
		#a("copy from dir parameter depth: ".$options["depth"]);
            // RFC 2518 Section 9.2, last paragraph
            return "409 Conflict";
        }


		#a("overwrite: ".$options["overwrite"]);
		#a("depth: ".$options["depth"]);
		#a("destfileexist: ".$destfileexist."  existingcol: ".$existing_col);
		# test the rights to the parent dest file
		if (bh_checkrights(bh_fpclean($infolder), $bhsession['username']) <= 1) { return "403 Forbidden"; }

		$fileobj->copyto($destfilepath);
		if ($move) {$fileobj->smartdeletefile();}

	        return (!$destfileexist && !$existing_col) ? "201 Created" : "204 No Content";         

	}

	function PROPFIND(&$options, &$files) {

		#a("_propfind_");
		#a($options['path']);

		global $bhsession;

	        // sanity check

		$fileexist = bh_user_file_exists($options['path']);
		
		if (!$fileexist) {
		  #a("il file non esiste");
           	  return false;
		}

		
		$filepath = bh_fpclean($options['path']);
		#a($filepath)
		$fileobj = new bhfile($filepath);
		$files['files'][] = $this->fileinfo($options['path']);

		#$fn = fopen('debuglog', 'a'); fputs($fn, $options['path']); fclose($fn);

		if ($fileobj->is_dir() == 1) {
			$filelist = $fileobj->loadfile();
			foreach ($filelist as $afile) {
				$files['files'][] = $this->fileinfo($afile['filepath']);
			}
		}
		
		return "200 OK";
	}
	
	function fileinfo($filepath) {

		#a("_fileinfo_");
		#a($filepath);

		$return = array();
		$filepath = bh_fpclean($filepath);
		
		$fileobj = new bhfile($filepath);
		
		$filename = bh_get_filename($filepath);
		$return['path'] = utf8_encode($filepath);
		$return['props'][] = $this->mkprop("getdisplayname", $filepath);
		$return['props'][] = $this->mkprop("displayname", $filepath);
		$return['props'][] = $this->mkprop("creationdate", $fileobj->fileinfo['createdate']);
		if (!empty($fileobj->fileinfo['moddate'])) {
			$return['props'][] = $this->mkprop("getlastmodified", $fileobj->fileinfo['moddate']);
		} else {
			$return['props'][] = $this->mkprop("getlastmodified", $fileobj->fileinfo['createdate']);
		}
		$return['props'][] = $this->mkprop("getcontentlength", $fileobj->fileinfo['filesize']);
		if ($fileobj->is_dir() == 1) {
			$return['props'][] = $this->mkprop('getcontenttype', "directory");
			#$return['props'][] = $this->mkprop('contenttype', "directory");
			$return['props'][] = $this->mkprop('resourcetype', 'collection');
		} else {
			$return['props'][] = $this->mkprop('getcontenttype', $fileobj->mimetype());
			#$return['props'][] = $this->mkprop('contenttype', $fileobj->mimetype());
			$return['props'][] = $this->mkprop('resourcetype', '');
		}
		return $return;
		
	}
	
	
	
}

function a($a) { $fn = fopen('debuglog', 'a'); fputs($fn, "\n".time().":".$a); fclose($fn); };

$webdav = new webdav();
$webdav->ServeRequest();
