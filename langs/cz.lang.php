<?php

/*
 * ByteHoard 2.1
 * Copyright (c) Andrew Godwin & contributors 2004
 *
 *   Language file
 *   $Id: en.lang.php,v 1.11 2005/07/28 22:38:50 andrewgodwin Exp $
 *
 */
 
#name Czech
#author Michal Sladek (msladek@koop.cz)
#description The czech language file.
#version of translation 0.1

$bhlang['module:main'] = "Úvodní strana";
$bhlang['module:login'] = "Přihlášení";
$bhlang['module:logout'] = "Odhlášení";
$bhlang['module:find'] = "Najít soubor";

$bhlang['title:login'] = "Přihlášení";
$bhlang['explain:login'] = "Zadejte prosím své uživatelské jméno a heslo pro přihlášení do systému.";
$bhlang['label:username'] = "Uživatel: ";
$bhlang['label:password'] = "Heslo: ";
$bhlang['button:login'] = "Přihlásit";

$bhlang['title:folders'] = "složky";

$bhlang['title:viewing_directory'] = "Prohlížení složky:";
$bhlang['error:not_a_dir'] = "Zadaná cesta není adresářem.";

$bhlang['error:nothing_in_toolbar'] = "Nemáte žádné položky v nástrojové liště. Jedná se o zásadní chybu; kontaktujte prosím svého administrátora.";

$bhlang['error:not_a_file'] = "Zadaný soubor není správného typu pro tuto akci.";
$bhlang['error:no_file_specified'] = "Tato akce vyžaduje soubor, podle všeho jste žádný neposkytnul(a)..";
$bhlang['title:viewing_file'] = "Soubor: ";

$bhlang['module:download'] = "Stahování";
$bhlang['moduledesc:download'] = "Stažení souboru";

$bhlang['module:delete'] = "Mazání";
$bhlang['moduledesc:delete'] = "Smazání souboru";

$bhlang['label:_files'] = " Soubory";

$bhlang['title:error'] = "Chyba";
$bhlang['explain:error_occured'] = "Došlo k chybě a ta byla zalogována. Pokud se domníváte, že se jedná o zásadní chybu, kontaktujte prosím administrátora.";

$bhlang['error:page_not_exist'] = "Požadovaná stránka neexistuje!";
$bhlang['error:file_not_exist'] = "Požadovaný soubor neexistujte!";
$bhlang['error:directory_no_exist'] = "Požadovaný adresář neexistuje!";

$bhlang['title:deleting_'] = "Mazání ";
$bhlang['explain:delete'] = "Jste si jistý, že chcete smazat tento soubor?";

$bhlang['button:delete_file'] = "Smazat soubor";
$bhlang['button:cancel'] = "Zrušit";
$bhlang['notice:file_deleted'] = "Soubor smazán.";

$bhlang['error:access_denied'] = "Nemáte oprávnění k provedení akce.";

$bhlang['title:upload'] = "Nahrání souborů";

$bhlang['explain:upload'] = "Vyberte prosím soubory, které chcete nahrát na server a potom klikněte na tlačítko Nahrát. <br><br>Pamatujte prosím, že během nahrávání souborů neuvidíte žádné změny na obrazovce; stav nahrávání však může ukazovat stavová lišta Vašeho prohlížeče.";
$bhlang['module:upload'] = "Nahrávání souborů";
$bhlang['moduledesc:upload'] = "Přenos souborů z Vašeho počítače.";

$bhlang['button:upload'] = "Nahrát";
$bhlang['label:uploading_to'] = "Nahrát do: ";
$bhlang['button:change_folder'] = "Změnit složku";
$bhlang['title:choose_folder'] = "Vyberte složku";

$bhlang['error:no_write_permission'] = "Nemáte práva k zápisu.";

$bhlang['module:url'] = "URL souboru";
$bhlang['moduledesc:url'] = "Zobrazení adresy souboru.";

$bhlang['explain:the_url_to_that_file_is_'] = "URL souboru je: ";
$bhlang['notice:file_#FILE#_upload_success'] = "Soubor #FILE# byl úspěšně nahrán na server.";

$bhlang['notice:logged_out'] = "Jste odhlášen(a) ze systému.";

$bhlang['log:#USER#_logged_out'] = "Odhlášení #USER#";
$bhlang['notice:logged_in_as_#USER#'] = "Jste přihlášen(a) jako #USER#. Vítejte.";
$bhlang['notice:login_failed'] = "Zadané uživatelské jméno a/nebo heslo je chybné.";
$bhlang['log:failed_login_#USER#'] = "Přihlášení uživatele #USER# se nezdařilo";
$bhlang['log:successful_login_#USER#'] = "Úspešné prihlášení uživatele #USER#";
$bhlang['log:#USER#_uploaded_#FILE#'] = "Nahrání souboru #FILE# uživatelem #USER#";

$bhlang['title:add_folder'] = "Vytvoření složky";
$bhlang['module:addfolder'] = "Vytvoření složky";
$bhlang['moduledesc:addfolder'] = "Vytvoření nové složky";
$bhlang['explain:add_folder'] = "Vytváříte novou složku. Vyberte stávající složku, ve které bude umístěna a pak zadejte jméno nové složky.";
$bhlang['button:add_folder'] = "Vytvořit složku";
$bhlang['label:folder_name'] = "Jméno složky: ";
$bhlang['label:create_in'] = "Vytvoř v: ";
$bhlang['log:#USER#_denied_#PAGE#'] = "Zamítnut přístup ke stránce #PAGE# uživateli #USER#";

$bhlang['log:#USER#_created_#FOLDER#'] = "Složka #FOLDER# vytvořena uživatelem #USER#";
$bhlang['notice:folder_created'] = "Složka vytvořena.";

$bhlang['notice:file_#FILE#_upload_failure'] = "Nahrání souboru #FILE# se nezdařilo.";
$bhlang['log:#USER#_failed_upload_#FILE#'] = "Nahrání souboru #FILE# uživatelem #USER# se nezdařilo.";

$bhlang['explain:edit'] = "Proveďte změny v níže otevřeném souboru a stiskněte Uložit pro uložení změn. Pamatujte, že pokud se nejedná o čistě textový soubor (ale např. dokument z textového editoru nebo list z tabulkového kalkulátoru), je pro Vás tento editor pravděpodobně nepoužitelný a snaha o uložení souboru může vést k jeho poškození.";
$bhlang['title:editing_#FILE#'] = "Edituji #FILE#";
$bhlang['button:save'] = "Uložit";

$bhlang['module:edit'] = "Editovat";
$bhlang['moduledesc:edit'] = "Editace souboru (v textovém režimu)";

$bhlang['notice:file_saved'] = "Soubor uložen.";
$bhlang['log:#USER#_modified_#FILE#'] = "Soubor #FILE# modifikován uživatelem #USER#";

$bhlang['module:htmledit'] = "HTML Editor";
$bhlang['moduledesc:htmledit'] = "Editace souboru (v HTML režimu)";

$bhlang['explain:htmledit'] = "Proveďte změny v níže otevřené stránce a stiskněte Uložit pro jejich zachování.";

$bhlang['label:copy_to'] = "Kopírovat do: ";
$bhlang['explain:copy'] = "Vyberte umístění, do kterého chcete kopírovat soubor, zvolte jeho nové jméno, pak stiskněte Kopírovat.";
$bhlang['label:new_name'] = "Nové jméno: ";
$bhlang['button:copy'] = "Kopírovat";
$bhlang['title:copying_#FILE#'] = "Kopíruji #FILE#";
$bhlang['log:#USER#_copied_#FILE#_to_#DEST#'] = "Soubor #FILE# zkopírován do #DEST# uživatelem #USER#";
$bhlang['notice:file_copied'] = "Soubor zkopírován.";
$bhlang['module:copy'] = "Kopírování";
$bhlang['moduledesc:copy'] = "Kopírování souboru do jiné složky nebo změna jeho jména";

$bhlang['title:uploading'] = "Nahrávání";
$bhlang['explain:uploading'] = "Nahrávání započalo. Operace může chvíli trvat.<br>Po jejím skončení se toto okno zavře.";

$bhlang['notice:you_must_be_admin'] = "Pro přístup do této oblasti musíte být administrátorem.";
$bhlang['title:__administration'] = " [Administrace]";

$bhlang['title:welcome_to_administration'] = "Vítejte v administraci ByteHoardu";
$bhlang['explain:welcome_to_administration'] = "Vítejte v administrační části ByteHoardu. Použijte odkazy v horní části pro výběr příslušné sekce.";

$bhlang['module:users'] = "Uživatelé";
$bhlang['moduledesc:users'] = "Administrace uživatelů a skupin";

$bhlang['title:user_administration'] = "Administrace uživatelů";
$bhlang['explain:user_administration'] = "Níže vyberte skupinu nebo položku Všichni pro prohlížení uživatelů v dané skupině  / všech uživatelů. Pro editaci uživatele klikněte na jeho uživatelské jméno.";

$bhlang['label:disk_space_used'] = "Využité místo na disku";
$bhlang['label:upload_bandwidth_used'] = "Využitá šířka pásma pro upload";
$bhlang['label:download_bandwidth_used'] = "Využitá šířka pásma pro download";

$bhlang['title:views'] = "pohledy";

$bhlang['module:sharing'] = "Sdílení";
$bhlang['moduledesc:sharing'] = "Sdílení s dalšími uživateli";

$bhlang['title:sharing_'] = "Sdílení ";
$bhlang['explain:sharing'] = "Zde můžete definovat, kdo další smí vidět soubor a co s ním může dělat.";
$bhlang['label:users'] = "Uživatelé";
$bhlang['label:groups'] = "Skupiny";
$bhlang['label:public'] = "Všichni";
$bhlang['explain:no_users_sharing_to'] = "Nesdílíte s žádnými uživateli.";
$bhlang['explain:no_groups_sharing_to'] = "Nesdílíte s žádnými skupinami.";

$bhlang['label:sharing_hidden'] = "Skrytý";
$bhlang['explain:sharing_hidden'] = "Tento soubor je skrytý.";

$bhlang['label:sharing_viewable'] = "K prohlížení";
$bhlang['explain:sharing_viewable'] = "Tento soubor je možné prohlížet a kopírovat, ne však měnit.";

$bhlang['label:sharing_writable'] = "K zápisu";
$bhlang['explain:sharing_writable'] = "Tento soubor je možné prohlížet, přesouvat, měnit nebo smazat.";

$bhlang['label:sharingfolder_hidden'] = "Skrytá";
$bhlang['explain:sharingfolder_hidden'] = "Tato složka je skrytá.";

$bhlang['label:sharingfolder_viewable'] = "K prohlížení";
$bhlang['explain:sharingfolder_viewable'] = "Tuto složku je možné prohlížet a kopírovat, ne však měnit.";

$bhlang['label:sharingfolder_writable'] = "K zápisu";
$bhlang['explain:sharingfolder_writable'] = "Tuto složku je možné kopírovat, přesouvat, měnit nebo smazat.";

$bhlang['label:change_to'] = "Změň na: ";

$bhlang['notice:permissions_changed'] = "Práva změněna";

$bhlang['label:add_user'] = "Přidat uživatele:";
$bhlang['label:add_group'] = "Přidat skupinu:";
$bhlang['button:add'] = "Přidat";

$bhlang['button:edit'] = "Editovat";
$bhlang['button:delete'] = "Smazat";

$bhlang['column:user_type'] = "Typ";
$bhlang['column:used_space'] = "Obsazené místo";
$bhlang['column:bandwidth_30_days'] = "Šířka pásma (Posledních 30 dní)";
$bhlang['column:actions'] = "Akce";
$bhlang['column:username'] = "Uživatel";

$bhlang['explain:delete_user'] = "Jste si jist(á), že chcete nevratně smazat tohoto uživatele (a jeho soubory)?";
$bhlang['button:delete_user'] = "Smazat uživatele";
$bhlang['button:delete_user_and_files'] = "Smazat uživatele a jeho soubory";

$bhlang['notice:user_deleted'] = "Uživatel smazán.";
$bhlang['notice:user_and_files_deleted'] = "Uživatel a jeho soubory smazáni.";

$bhlang['title:settings'] = "Nastavení";
$bhlang['explain:settings'] = "Zde můžete měnit základní nastavení Bytehoardu. Vyberte a změnte volby dle Vašich potřeb a klikněte na 'Ulož nastavení' pro aplikaci změn.";

$bhlang['label:settings_usetrash'] = "Používat odpadkový koš?";
$bhlang['explain:settings_usetrash'] = "Zvolíte-li ano, soubory nebudou trvale smazány, ale přesouvány do složky Odpadkový koš.";

$bhlang['label:yes'] = "Ano";
$bhlang['label:no'] = "Ne";

$bhlang['button:save_settings'] = "Uložit nastavení";

$bhlang['module:settings'] = "Nastavení";
$bhlang['moduledesc:settings'] = "Změna základních nastavení";

$bhlang['label:settings_sitename'] = "Název serveru";
$bhlang['explain:settings_sitename'] = "Název tohoto serveru (zobrazeno na WWW stránkách a v e-mailech)";

$bhlang['label:settings_limitthumbs'] = "Náhledy jen u malých souborů?";
$bhlang['explain:settings_limitthumbs'] = "Zvolíte-li ne, velké obrázky mohou způsobit zpomalení systému nebo zastavit vykreslování náhledů.";

$bhlang['notice:settings_saved'] = "Nastavení uložena.";

$bhlang['title:delete_user'] = "Smazat uživatele";
$bhlang['title:edit_user'] = "Editovat uživatele";

$bhlang['title:settings'] = "Nastavení";

$bhlang['explain:edit_user'] = "Proveďte změny v nastavení uživatele a klikněte na 'Ulož uživatele' pro jejich aplikaci. <br>Pokud nechcete měnit heslo, nechte příslušné kolonky prázdné.";

$bhlang['subtitle:details'] = "Detaily";

$bhlang['title:editing_user_'] = "Editace uživatele: ";
$bhlang['label:email'] = "E-mail:";
$bhlang['label:full_name'] = "Jméno a příjmení:";
$bhlang['label:user_type'] = "Typ uživatele:";

$bhlang['value:guest'] = "Host";
$bhlang['value:normal'] = "Normální";
$bhlang['value:admin'] = "Administrátor";

$bhlang['subtitle:password'] = "Heslo";
$bhlang['label:new_password'] = "Nové heslo:";
$bhlang['label:repeat_new_password'] = "Zopakujte nové heslo:";

$bhlang['button:save_user'] = "Uložit uživatele";

$bhlang['error:passwords_dont_match'] = "Hesla nejsou shodná!";
$bhlang['notice:user_updated'] = "Uživatel změněn!";

$bhlang['title:signup'] = "Registrace";
$bhlang['explain:signup'] = "Vyplňte níže uvedené kolonky a klikněte na 'Registrovat!' pro odeslání žádosti o registraci.";

$bhlang['label:repeat_password'] = "Zopakovat heslo:";
$bhlang['button:signup'] = "Registrovat!";

$bhlang['module:signup'] = "Registrace";
$bhlang['moduledesc:signup'] = "Registrace uživatelského účtu";

$bhlang['error:username_in_use'] = "Toto uživatelské jméno je již použito. Zkuste jiné.";
$bhlang['notice:signup_successful_can_login'] = "Úspěch! Nyní se můžete přihlásit pomocí uživatelského jména a hesla, které jste zadal(a).";
$bhlang['log:user_signed_up_'] = "Uživatel se úspěšně zaregistroval s uživatelským jménem ";
$bhlang['error:password_empty'] = "Nezadal(a) jste heslo!";

$bhlang['notice:file_description_saved'] = "Popis souboru uložen!";
$bhlang['title:editing_description_#FILE#'] = "Edituji popis souboru #FILE#";
$bhlang['explain:editdesc'] = "Níže můžete editovat popis souboru (objeví se při výpisu složky). Ponecháte-li pole prázdné, popis se doplní automaticky.";

$bhlang['button:savedesc'] = "Uložit popis";
$bhlang['module:editdesc'] = "Editace popisu";
$bhlang['moduledesc:editdesc'] = "Změna popisu souboru";

$bhlang['title:appearance']  = "Vzhled";
$bhlang['explain:appearance'] = "Zde můžete měnit vzhled ByteHoard pomocí volby jednoho z instalovaných skinů. Vyberte si preferovaný vzhled pomcí tlačítka 'Použij tento skin'; skin, který v tuto chvíli používate je označen jako 'Aktuální skin', a nemá toto tlačítko k dispozici.<br><br>Pamatujte, že administrační centrum vypadá pořád stejně; mustíte přejít do hlavního systému, aby jste viděl(a) změny.";

$bhlang['label:author'] = "Autor: ";

$bhlang['module:appearance'] = "Vzhled";
$bhlang['moduledesc:appearance'] = "Editace vzhledu systému";

$bhlang['explain:current_skin'] = "Aktuální skin";

$bhlang['button:use_this_skin'] = "Použít tento skin";
$bhlang['notice:skin_changed'] = "Skin změněn.";

$bhlang['label:settings_signupmoderation'] = "Schvalovat uživatelské registrace?";
$bhlang['explain:settings_signupmoderation'] = "Nastavíte-li na ano, musí administrátor schvalovat všechny nové uživatele.";

$bhlang['error:validation_link_wrong'] = "Lituji, ověřovací odkaz na který jste kliknul(a) není platný. Mohl již vypršet; v takovém případě se zkuste znovu zaregistrovat.";
$bhlang['log:user_validated_'] = "Uživatel úspešně ověřil uživatelské jméno ";
$bhlang['log:user_signup_m_pending_'] = "Na schválení čeká uživatelské jméno ";

$bhlang['notice:moderation_now_pending'] = "Vaše registrace byla úspěšně ověřena a byla postoupena administrátorovi pro schválení. O přijetí či zamítnutí žádosti budete informován(a).";
$bhlang['notice:do_email_validation'] = "Na Vámi zadanou adresu byl zaslán e-mail s infomacemi, jak ověřit Vaši adresu pro další pokračování registrace.";

$bhlang['emailsubject:registration_validation'] = "#SITENAME# ověření registrace";


# Note to translators: Just the stuff inside the double quotes, it's allowed to go over multiple lines.
# Another note: Lines beginning with a hash (#) are ignored. So this line is ignored. And the one above.
$bhlang['email:registration_validation'] = "Vaše žádost o účet v současné době čeká na ověření.

Pro ověření Vašeho účtu, klikněte prosím na následující odkaz:

#LINK#

Pokud ověření svého ůčtu neprovedete během sedmi dní, Vaše žádost o registraci bude smazána.";




$bhlang['error:email_error'] = "V současné době máme problémy s elektronickou poštou; Vaše žádost teď nemůže být dokončena. Zkuste prosím později.";

$bhlang['notice:validation_already_done_pending_approval'] = "Již jste provedl(a) ověření Vašeho účtu! V současné době čeká na schválení.";

$bhlang['title:registrations_administration'] = "Administrace registrací";
$bhlang['module:registrations'] = "Registrace";
$bhlang['moduledesc:registrations'] = "Schválení či zamítnutí registrací nových uživatelů";
$bhlang['explain:registration_administration'] = "Toto je seznam všech ověřených žáostí o registraci čekajících na schválení. Ukazuje detaily všech potenciálních uživatelů, společně s volbami na přijetí nebo zamítnutí jejich žádostí. <br>Buďte obezřetný/á, pro žádnou z akcí není vyžadováno potvrzení.";

$bhlang['notice:registration_moderation_off'] = "Schvalování registrací je vypnuto; všichni uživatelé jsou schváleni automaticky, jakmile provedou ověření. Chcete-li schvalovaní zapnout, přejděte do sekce Nastavení a změňte položku 'Schvalovat uživatelské registrace?' na 'Ano'.";

$bhlang['button:reject'] = "Zamítnout";
$bhlang['button:accept'] = "Schválit";

$bhlang['notice:#USER#_accepted'] = "#USER# schválen.";
$bhlang['notice:#USER#_rejected'] = "#USER# zamítnut.";

$bhlang['emailsubject:registration_accepted'] = "Registrace schválena pro #SITENAME#";
$bhlang['email:registration_accepted'] = "Vaše registrace byla schválena administrátorem.

Můžete se přihlásit s následujícími údaji:
Uživatel: #USERNAME#
Heslo: To, které jste zadal(a) v registračním formuláři.

Vaše heslo Vám nemůžeme sdělit, protože je v naší databázi uloženo v zašifrované podobě, můžeme ho však změnit; bližší informace se dozvíte na našich stránkách.

Doufáme, že budete s naší službou spokojeni.";

$bhlang['emailsubject:registration_rejected'] = "Registrace zamítnuta pro #SITENAME#";
$bhlang['email:registration_rejected'] = "Vaše registrace nebyla schválena; uživatelský účet Vám nebude vytvořen.

Samozřejmě můžete zažádat znovu; avšak pokud Vaše žádost nebyla přijata na poprvé, je nepravděpodobné, že tentokrát uspějete.";


$bhlang['error:registration_doesnt_exist'] = "Tato registrace už neexistuje. Patrně ji schválil nebo zamítnul někdo jiný.";

$bhlang['error:username_too_long'] = "Uživatelské jméno je příliš dlouhé. Musí mít méně než 255 znaků.";

$bhlang['button:go'] = "Odeslat";
$bhlang['button:request_reset'] = "Zrušit žádost";

$bhlang['title:recover_password'] = "Reset hesla";
$bhlang['explain:recover_password'] = "Tento formulář slouží k obnově uživatelského jména nebo hesla. Vložte své uživatelské jméno nebo heslo do příslušné kolonky a klikněte na 'Odešli' pro odeslání požadavku. Obdržíte e-mail, který bude obsahovat Vaše uživatelské jméno a odkaz k resetu hesla.";

$bhlang['text:or'] = "nebo";

$bhlang['module:passreset'] = "Ztráta hesla";
$bhlang['moduledesc:passreset'] = "Obnova Vašeho uživatelského jména nebo reset hesla";

$bhlang['emailsubject:passreset_request'] = "Žádost o reset hesla pro #SITENAME#";
$bhlang['email:passreset_request'] = "Vy, případně někdo, kdo se za Vás vydává, jste zažádal(a) o změnu hesla pro tuto stránku. Pokud skutečně chcete Vaše stávající heslo vyresetovat, klikněte na následující odkaz:

#LINK#

Pokud jste nežádal(a) o reset hesla, ignorujte prosím tento e-mail. Žádost bude za 48 hodin smazána.";

$bhlang['error:username_doesnt_exist'] = "Toto uživatelské jméno neexistuje v naší databázi!";
$bhlang['error:email_doesnt_exist'] = "Tato e-mailová adresa neexistuje v naší databázi!";

$bhlang['emailsubject:passreset_u_request'] = "Žádost o uživatelské jméno a heslo pro #SITENAME#";
$bhlang['email:passreset_u_request'] = "Vy, případně někdo, kdo se za Vás vydává, jste zažádal(a) o připomenutí přihlašovacích údajů pro tuto stránku.

Vaše uživateslké jméno je: #USERNAME#

Pro reset Vašeho hesla, klikněte prosím na následující odkaz:

#LINK#

Pokud jste nežádal(a) o reset hesla, ignorujte prosím tento e-mail. Žádost bude za 48 hodin smazána.";

$bhlang['notice:passreset_request_sent'] = "E-mail obsahující další instrukce byl zaslán na Vaši adresu.";

$bhlang['error:passreset_link_invalid'] = "Odkaz je neplatný - buď vypršel nebo jste ho špatně zadal(a) do svého prohlížeče.";

$bhlang['emailsubject:passreset_new_password'] = "Vaše nové heslo pro #SITENAME#";
$bhlang['email:passreset_new_password'] = "Vaše nové heslo pro stránky je:

#PASSWORD#

Toto heslo bylo náhodně vygenerováno a můžete jej změnit po přihlášení.";

$bhlang['notice:passreset_new_password_sent'] = "Nové heslo bylo zasláno na Vaši e-mailovou adresu.";

$bhlang['error:username_invalid'] = "Toto uživatelské jméno je neplatné.";

$bhlang['error:systemwrong'] = "Zásadní chyba: Nastavení Vašeho systému znemožňuje fungování této funkce.";

$bhlang['title:options'] = "Nastavení";
$bhlang['module:options'] = "Nastavení";
$bhlang['moduledesc:options'] = "Změna Vašich nastavení";
$bhlang['explain:options'] = "Zde můžete změnit některá nastavení, která se týkají vzhledu a fungování systému, své heslo a kontaktní informace.";
$bhlang['title:change_password'] = "Změna hesla";
$bhlang['title:interface_options'] = "Nastavení vzhledu";
$bhlang['title:profile'] = "Profil uživatele";

$bhlang['label:old_password'] = "Staré heslo: ";
$bhlang['button:change_password'] = "Změnit heslo";
$bhlang['error:old_password_invalid'] = "Vaše staré heslo je chybné! Zkuste jej zadat znovu.";
$bhlang['notice:password_changed'] = "Heslo bylo úspěšně změněno!";

$bhlang['error:unknown'] = "Došlo k neznámé chybě. Teď může být vhodná chvíle k jejímu nahlášení.";
$bhlang['warning:blank_password'] = "Používáte prázdné heslo! To je hodně špatný nápad.";

# These are all options! We're not going to collect all this inforation about every user.
# The administrator will be able to choose which ones to allow.
$bhlang['profile:email'] = "E-mailová adresa";
$bhlang['profile:fullname'] = "Jméno a příjmení";
$bhlang['profile:website'] = "WWW stránka";
$bhlang['profile:telephone'] = "Telefonní číslo";
$bhlang['profile:fax'] = "Faxové číslo";
$bhlang['profile:jabber'] = "Jabber ID";
$bhlang['profile:icq'] = "ICQ#";
$bhlang['profile:msn'] = "MSN ID";
$bhlang['profile:aim'] = "AIM ID";
$bhlang['profile:yahoo'] = "Yahoo! ID";
$bhlang['profile:nickname'] = "Přezdívka";
$bhlang['profile:position'] = "Funkce";
$bhlang['profile:location'] = "Lokalita";
$bhlang['profile:gender'] = "Pohlaví";
$bhlang['profile:address'] = "Adresa";
$bhlang['profile:postcode'] = "PSČ";
$bhlang['profile:zipcode'] = "Zipcode";
$bhlang['profile:nationality'] = "Národnost";
$bhlang['profile:latitude'] = "Zeměpisná šířka";
$bhlang['profile:longitude'] = "Zeměpisná délka";
$bhlang['profile:occupation'] = "Povolání";
$bhlang['profile:status'] = "Status";

$bhlang['button:save_profile'] = "Uložit profil";
$bhlang['notice:profile_saved'] = "Profil uložen.";

$bhlang['error:cannot_determine_update_server'] = "Aktualizační server nebyl nalezen. Je pravděpodobné, že server ByteHoardu neběží; zkuste prosím později.";
$bhlang['error:cannot_download_package'] = "Nelze stáhnout instalační soubor ze vzdáleného serveru.";
$bhlang['error:system_setup_wrong'] = "Systém nedokáže stáhnout požadované soubory. Musíte to udělat ručně.";
$bhlang['error:cannot_write_to_package'] = "Nemohu zapisovat do instalačního souboru. Zkontrolujte prosím nastavení práv na adresář ByteHoardu.";

$bhlang['title:folder_files'] = "Soubory";
$bhlang['title:folder_actions'] = "Práce se složkami";
$bhlang['module:deletefolder'] = "Smaž";
$bhlang['moduledesc:deletefolder'] = "Smazání složky a obsažených souborů";
$bhlang['module:copyfolder'] = "Kopíruj";
$bhlang['moduledesc:copyfolder'] = "Kopírování složky do jiného umístění";
$bhlang['module:sharingfolder'] = "Sdílení";
$bhlang['moduledesc:sharingfolder'] = "Sdílení složky s ostatními uživateli";
$bhlang['notice:folder_deleted'] = "Složka smazána";

$bhlang['error:cannot_delete_that'] = "Nemůžete smazat tuto složku nebo soubor.";

$bhlang['button:delete_folder'] = "Smazat složku";
$bhlang['explain:deletefolder'] = "Opravdu chcete smazat tuto složku a všechen její obsah?";

$bhlang['label:settings_fromemail'] = "Od systému: Adresa: ";
$bhlang['explain:settings_fromemail'] = "Odchozí adresa, kterou budou mít nastaveny všechny systémové e-maily.";
$bhlang['label:settings_imageprog'] = "Použitý grafický program: ";
$bhlang['explain:settings_imageprog'] = "Musí být buď 'imagemagick' nebo 'gd'.";
$bhlang['label:settings_syspath_convert'] = "Cesta ke 'convert': ";

# Translators; Leave $"."PATH as it is.
$bhlang['explain:settings_syspath_convert'] = "Vyžadováno jen pokud zvolíte 'imagemagick' jako grafický program.<br>Můžete napsat 'convert' pokud je program ve Vaší systémové proměnné $"."PATH.";

$bhlang['label:settings_fileroot'] = "Adresář virtuálního filesystému: ";
$bhlang['explain:settings_fileroot'] = "Adresář, do kterého ByteHoard ukládá soubory.<br><b>NEMĚŇTE HO</b> pokud nevíte jistě co děláte.<br>Cesta NESMÍ BÝT ukončena adresářovým lomítkem.";

$bhlang['error:no_gd'] = "GD není instalován, ačkoli je zvolen pro vytváření náhledů. To je zásadní chyba.";

$bhlang['explain:sharingfolder'] = "Zde můžete nastavit, kdo další může složku vidět a co s ní smí dělat. <br>Pamatujte, že každá změna provedená složce, ovlivní všechny soubory v ní uložené.<br>Z tohoto důvodu nedoporučujeme sdílení Vašeho hlavního adresáře.";

$bhlang['label:sharingfolder_owner'] = "Vlastník";
$bhlang['explain:sharingfolder_owner'] = "Má plnou kontrolu nad složkou.";
$bhlang['label:sharing_owner'] = "Vlastník";
$bhlang['explain:sharing_owner'] = "Má plnou kontrolu nad souborem.";

$bhlang['error:permissions_self'] = "Nemůžete změnit svá vlastní práva.";
$bhlang['notice:permissions_changed'] = "Práva změněna.";
$bhlang['notice:permissions_group_added'] = "Skupina přidána.";
$bhlang['notice:permissions_user_added'] = "Uživatel přidán.";
$bhlang['notice:permissions_group_deleted'] = "Skupina smazána.";
$bhlang['notice:permissions_user_deleted'] = "Uživatel smazán.";

$bhlang['label:delete_user'] = "Smazání uživatele: ";
$bhlang['label:delete_group'] = "Smazání skupiny: ";

$bhlang['button:return'] = "Návrat";

$bhlang['module:returntofolder'] = "Návrat do složky";
$bhlang['moduledesc:returntofolder'] = "Zobrazení složky, ve které je soubor uložen";

$bhlang['notice:folder_copied'] = "Složka zkopírována.";

$bhlang['module:filelink'] = "SouborováPošta";
$bhlang['moduledesc:filelink'] = "Vytvoření dočasného odkazu na soubor a jeho odeslání e-mailem";
$bhlang['error:no_filepath'] = "Zásadní chyba: Nebyla zadána cesta k souboru.";

$bhlang['title:filemail'] = "SouborováPošta";
$bhlang['explain:filemail'] = "SouborováPošta Vám umožní odeslat e-mailem dočasný odkaz na tento soubor, který po určité době vyprší. <br><br>Vyplňte níže uvedené kolonky: adresu příjemnce e-mailu, subjekt a případně zprávu. Odkaz na soubor bude automaticky přidán na konec e-mailu. Chcete-li e-mail odeslat více příjemcům, oddělte jejich adresy čárkou.";

$bhlang['label:subject'] = "Subjekt: ";
$bhlang['label:message'] = "Zpráva: ";

$bhlang['error:no_emailaddr'] = "Nezadal(a) jste e-mailovou adresu.";
$bhlang['error:no_emailsubj'] = "Nezadal(a) jste subjekt e-mailu.";
$bhlang['error:invalid_email_#EMAIL#'] = "E-mailová adresa '#EMAIL#' je neplatná.";

$bhlang['notice:email_sent'] = "E-mail byl úspěšně odeslán.";
$bhlang['notice:email_sent_to_#EMAIL#'] = "E-mail byl úspěšně odeslán na adresu #EMAIL#.";

$bhlang['text:days'] = " dní";
$bhlang['label:expires_in'] = "Vyprší za: ";

$bhlang['text:max_#NUM#_days'] = "(nejvíce #NUM# dní)";

$bhlang['error:expires_invalid'] = "Čas expirace je neplatný. Musí být zadáno kladné číslo.";
$bhlang['error:expires_too_much'] = "Čas expirace je delší než povolený limit.";

$bhlang['label:settings_maxexpires'] = "Maximální počet dní expirace SouborovéPošty: ";
$bhlang['explain:settings_maxexpires'] = "Nejvyšší počet dní, který smí být nastaven pro platnost odkazů SouborovéPošty.";

$bhlang['error:no_filecode'] = "Nezadali jste odkaz s identifikátorem souboru.";
$bhlang['error:filecode_invalid'] = "Zadaný odkaz je buď neplatný nebo vypršela jeho platnost.";

$bhlang['title:filelinks'] = "Administrace souborových odkazů";
$bhlang['module:filelinks'] = "SouborováPošta";
$bhlang['moduledesc:filelinks'] = "Administrace odkazů SouborovéPošty";
$bhlang['explain:filelinks'] = "Tato stránka poskytuje přehled aktivních odkazů SouborovéPošty. <br>Jsou řazeny dle uživatelského jména. Pod každým uživatelem je seznam jeho odkazů, společně s příslušnou e-mailovou adresou, souborem a časem do vypršení odkazu. <br>Odkaz si můžete prohlédnout kliknutím na 'Odkaz' vedle každého záznamu, nebo ho deaktivovat kliknutím na 'Smaž'.";

$bhlang['column:expires_in'] = "Vyprší za";
$bhlang['column:email'] = "E-mailové adresa";
$bhlang['column:file'] = "Soubor";

$bhlang['button:link'] = "Odkaz";

$bhlang['notice:filelink_deleted'] = "Odkaz SouborovéPošty smazán.";

$bhlang['log:filelink_denied'] = "Zamítnut přístup k #FILELINK#";
$bhlang['log:filelink_accessed'] = "Přístup k #FILEPATH# prostřednictvím odkazu [#FILELINK#].";

$bhlang['label:settings_authmodule'] = "Autentikační modul: ";
$bhlang['explain:settings_authmodule'] = "Tento modul slouží k autentikaci uživatelů. Ponechte výchozí 'bytehoard.inc.php' pokud nevyžadujete jinou autentikaci než vůči ByteHoardu.";

$bhlang['button:send'] = "Odeslat";
$bhlang['button:back'] = "Zpět";


$bhlang['label:settings_baseuri'] = "URL adresa ByteHoardu (volitelné): ";
$bhlang['explain:settings_baseuri'] = "URL adresa hlavního adresáře ByteHoardu (musí být zakončena lomítkem). Je volitelná; necháte-li pole prázdné, použije se automaticky detekovaná hodnota.";

$bhlang['title:overwriting_'] = "Přepisuji ";
$bhlang['explain:overwrite'] = "Jméno nahrávaného souboru se shoduje se jménem jiného existujícího souboru. Co chcete dělat?";

$bhlang['label:linkonly'] = "Vytvořit pouze odkaz (nezasílat ho e-mailem): ";

$bhlang['text:link__expire_in_#EXPIRE#'] = "Odkaz (Vyprší za #EXPIRE# dní): ";

$bhlang['label:html'] = "HTML: ";
$bhlang['label:bbcode'] = "BBkód: ";
$bhlang['title:image_tags'] = "Tagy obrázku";

$bhlang['error:email_empty'] = "Nezadal(a) jste e-mailovou adresu.";

$bhlang['email:filemail_footer'] = "---------------------------------------------
Soubor bude k dispozici ke stažení do #DATE#

Příloha: #FILENAME#, #FILESIZE# (MD5: #MD5#)
#LINK#

V tomto e-mailu odeslánem z #SYSTEMNAME# jste obdržel(a) odkaz na připojený soubor. 
Pro stažení souboru, klikněte prosím na odkaz.";

$bhlang['label:downloadnotice'] = "Odesílání notifikačních e-mailů o stahování: ";

$bhlang['emailsubject:filemail_link_accessed'] = "Oznámení o přístupu k odkazu SouborovéPošty pro soubor (#FILENAME#)";

$bhlang['email:filemail_link_accessed'] = "Toto je automaticky odeslané oznámení.

Byl zaznamenán přístup k jednomu z Vašich odkazů SouborovéPošty. Když jste odkaz vytvářel(a), požadoval(a) jste oznámení o každém přístup na tento odkaz.

K souboru (#FILEPATH#) byl zaznamenán přístup v #TIME#, z IP adresy #IP#. Odkaz jste odesílal(a) na e-mail #EMAIL#.

Upozornění: Tento odkaz vyprší #EXPIRES#.";




$bhlang['install:title:bytehoard_installation'] = "Instalace ByteHoardu";

$bhlang['install:title:page_not_found'] = "Stránka nebyla nalezena";
$bhlang['install:error:page_not_found'] = "Stránka neexistuje!";
$bhlang['install:title:menu'] = "Nabídka";
$bhlang['install:menu:install'] = "Instalace ByteHoardu";
$bhlang['install:menu:upgrade'] = "Upgrade ByteHoardu";
$bhlang['install:menu:documentation'] = "Dokumentace";
$bhlang['install:menu:systeminfo'] = "Systémové informace";

$bhlang['install:text:install_intro'] = "Vítá Vás ByteHoard ".$bhconfig['version'].".<br><br>Následující stránky Vás povedou procesem instalace. Nastavení je relativně jednoduché; systém se Vás pokusí vést jak nejvíce to bude možné. Připravte si informace potřebné pro přihlášení k Vaší databázi a ujistěte se, že Váš systém splňuje všechny požadavky pro instalaci ByteHoardu.<br><br>Pokrčováním v instalaci vyjadřujete souhlas s licenčními podmínkami tohoto programu - GNU GPL v2 (její kopii můžete nalézt v souboru LICENSE, který je součástí instalačního archivu nebo jí najdete na adrese <a href='http://www.gnu.org/copyleft/gpl.html'>http://www.gnu.org/copyleft/gpl.html</a>.<br><br>Pro podporu, aktualizace, rozšíření a novinky o vývoji navštivte naší stránku na adrese <a href='http://bytehoard.org'>bytehoard.org</a>";

$bhlang['install:title:introduction'] = "Úvod";
$bhlang['install:title:systemchecks'] = "Kontrola systému";
$bhlang['install:text:checking_system'] = "Kontroluji systém...";
$bhlang['install:label:php'] = "PHP";
$bhlang['install:label:extensions'] = "Rozšíření";
$bhlang['install:label:external_progs'] = "Externí programy";
$bhlang['install:label:filesystem'] = "Souborový systém";
$bhlang['install:check:php'] = "Verze PHP: ";
$bhlang['install:check:safe_mode'] = "Safe_mode je vypnut: ";
$bhlang['install:check:pcre'] = "PCRE: ";
$bhlang['install:check:gd'] = "GD: ";
$bhlang['install:check:imagemagick'] = "ImageMagick: ";

$bhlang['install:check:php5'] = "PHP5 (není zcela otestováno)";
$bhlang['install:check:failed'] = "Selhalo";
$bhlang['install:check:failedoption'] = "Selhalo (volitelné)";
$bhlang['install:check:ok'] = "OK";

$bhlang['install:check:failedphp'] = "Selhalo (Je třeba PHP 4.2 nebo novější)";
$bhlang['install:check:failedsafemode'] = "Selhalo (safe_mode je aktivní)";
$bhlang['install:check:failedperm'] = "Selhalo (soubor není zapisovatelný, zkontrolujte oprávnění)";

$bhlang['install:check:config.inc.php'] = "config.inc.php má práva k zápisu: ";
$bhlang['install:check:cache'] = "cache/ má práva k zápisu: ";

$bhlang['install:text:checksok'] = "Všechny kontroly proběhly. V pořádku.";
$bhlang['install:text:checkswarnings'] = "Zdá se, že je zde pár chyb, ale nic vážného. Můžete buď pokračovat, nebo zkusit doinstalovat chybějící komponenty. Pokud s tím chcete pomoci, neváhejte s návštěvou stránek ByteHoardu (bytehoard.org) a zeptejte se nás.";
$bhlang['install:text:checksfatals'] = "Jeden či více zásadních testů selhalo. Vyřešte je prosím, než zkusíte pokračovat. Pokud chcete s kterýmko-li z problémů pomoci, zastavte se na bytehoard.org a mi Vám rádi pomůžeme.";
$bhlang['install:text:thumbnails_disabled'] = "Upozornění: Náhledy nebudou povoleny. Chcete-li je použít, naistalujte GD nebo ImageMagick.";
$bhlang['button:next'] = "Pokračovat";

$bhlang['install:title:choose_database'] = "Výběr databáze";
$bhlang['install:title:configure_database'] = "Konfigurace databáze";

$bhlang['install:title:system_information'] = "Systémové informace";

$bhlang['install:configdb:intro'] = "Teď musíte poskytnout informace pro připojení k Vaší databázi. Pokud je neznáte, promluvte si se svým systémovým administrátorem. Pokud tato cesta selže, zkuste se obrátit na nás.";
$bhlang['install:configdb:nont_needed'] = "Tento databázový modul nemá konfiguraci. Můžete pokračovat dalším krokem.";
$bhlang['install:title:save_database_configuration'] = "Ukládám konfiguraci databáze...";
$bhlang['install:writeconfig:saved'] = "Konfigurace databáze byla úspěšně uložena.";
$bhlang['install:writeconfig:not_saved'] = "Došlo k chybě při ukládání konfigurace databáze. Zkontrolujte prosím přístupová práva k adresáři ByteHoardu.";
$bhlang['install:title:init_database'] = "Inicializace databáze";

$bhlang['install:title:test_database_configuration'] = "Testuji konfiguraci databáze...";
$bhlang['install:testdb:ok'] = "Konfigurace vypadá v pořádku.";
$bhlang['install:testdb:failed'] = "Nemohu se připojit k databázi! Výpis chyby následuje.";
$bhlang['error:error_creating_table'] = "Chyba při vytváření tabulky: ";
$bhlang['install:initdb:database_initialised'] = "Databáze inicializována.";

$bhlang['install:title:set_paths'] = "Nastavení cest";

$bhlang['install:paths:intro'] = "Teď je třeba nastavit cesty pro ByteHoard. Musíte zvolit URL systému a adresář, kam budou ukládány soubory uživatelů.<br><br>Vaše URL se instalátor pokusí uhádnout; každopádně ho však zkontrolujte, někdy se v něm objeví nepatřičné části nebo v něm chybí lomítka. Korektní URL by mělo mít http:// nebo https:// prefix a lomítko (/) na konci.<br><br>Adresář pro ukládání souborů je defaultně nastaven jako podadresář hlavního adresáře ByteHoardu nazvaný 'filestorage', což není úplně bezpečné. Pokud můžete, vytvořte jiný adresář mimo adresářovou strukturu webové aplikace, nastavte mu příslušná práva a použijte cestu k němu. <br>Pamatujte prosím, že relativní cesta bude interpretována vzhledem k hlavnímu adresáři ByteHoardu.";

$bhlang['install:paths:system_url'] = "URL systému: ";
$bhlang['install:paths:file_storage_directory'] = "Adresář pro ukládání souborů: ";

$bhlang['value:enabled'] = "Ne";
$bhlang['value:disabled'] = "Ano (Uživatel se nemůže přihlásit)";

$bhlang['label:disabled'] = "Uživatel zablokován?: ";

$bhlang['install:title:save_paths'] = "Ukládám cesty...";

$bhlang['install:savepaths:message'] = "Cesty uloženy.";

$bhlang['install:savepaths:error_baseuri'] = "Toto je neplatné URL systému. Ujistěte se prosím, že URL obsahuje http:// nebo https:// a končí lomítkem.";
$bhlang['install:savepaths:error_fileroot'] = "Toto je neplatná adresářová cesta.";

$bhlang['install:title:create_administrator'] = "Vytvoření administrátora";

$bhlang['notice:login_failed_disabled'] = "Přihlášení selhalo: Váš účet byl zablokován.";

$bhlang['log:failed_login_disabled_#USER#'] = "#USER# nebyl přihlášen (zablokován)";

$bhlang['title:add_user'] = "Přidání uživatele";
$bhlang['explain:add_user'] = "Použijte tento formulář k přidání nového uživatele. Musíte zadat uživatelské jméno, heslo a e-mailovou adresu; ostatní kolonky jsou volitelné.";
$bhlang['button:add_user'] = "Přidat uživatele";
$bhlang['notice:user_added'] = "Uživatel úspěšně přidán.";
$bhlang['label:homedir'] = "Domovský adresář: ";
$bhlang['value:normal_homedir'] = "Podadresář pod / (výchozí)";
$bhlang['value:root_homedir'] = "/ (žádný domovský adresář)";
$bhlang['label:groups'] = "Počáteční skupiny (oddělte čárkami): ";
$bhlang['module:adduser'] = "Přidání uživatele";
$bhlang['moduledesc:adduser'] = "Přidání nového uživatele do systému.";

$bhlang['install:createadmin:explain'] = "Uživatel administrátor byl vytvořen. Prosím uchovejte jeho přihlašovací údaje na bezpečném místě. Jakmile se přihlásíte, můžete změnit jeho heslo, nebo vytvořit další uživatele.";


$bhlang['install:finish:explain'] = "Instalace je dokončena. Můžete se přihlásit výše uvedeným uživatelským jménem a heslem administrátora.<br><br><b>Pamatujte: MUSÍTE kompletně smazat adresář install/ než začnete ByteHoard používat. Kdokoli má přístup do tohoto adresáře, získá jméno a heslo administrátora a bude schopen přistupovat do databáze ByteHoardu a možná i dalších aplikací na tomto počítači. Ponechání tohoto adresáře představuje velké bezpečnostní riziko. Pokud potřebujete provést reinstalaci nebo upgrade ByteHoardu, jednoduše rozbalte adresář install/ přímo z instalačního archivu.</b><br><br>Doporučujeme Vam přihlásit se nejprve do administrační sekce ByteHoardu a změnit nastavení systému dle Vašich potřeb. URL hlavní sekce a administrační sekce jsou zobrazena níže.";
$bhlang['install:finish:label:url'] = "URL systému: ";
$bhlang['install:finish:label:adminurl'] = "URL administrační sekce: ";

$bhlang['error:signup_disabled'] = "Registrace byly zablokovány.";
$bhlang['label:settings_signupdisabled'] = "Zablokovat registrace?: ";
$bhlang['explain:settings_signupdisabled'] = "Pokud toto zaškrtnete volba 'Registrace' bude vykreslena jako nepřístupná.";

$bhlang['install:title:complete'] = "Hotovo";

$bhlang['install:title:bytehoard_upgrade'] = "Upgrade ByteHoardu";

$bhlang['install:update:intro'] = "Vítejte v ByteHoard Updateru. Tento program zaktualizuje Vaši starou instalaci ByteHoardu.<br><br>Pamatujte, že před upgradem je třeba přesunout nebo smazat obsah adresáře s původní verzí ByteHoradu kromě souboru <i>config.inc.php</i> a adresářů <i>filestorage/</i> nebo <i>userfiles/</i> a do stejného adresáře nakopírovat soubory nové verze (možná je budete muset přesunout z adresáře, který vznikl rozbalením instalačního archivu, nazvaném pravděpodobně <i>bytehoard-".str_replace(" ","-",strtolower($bhconfig['version']))."/</i>.<br><br>Před provedením této operace si prosím vytvořte kopii souborů a databáze starého ByteHoardu; to by jste sice měli dělat před instalací libovolného nového softwaru, ale v tomto případě nemusí upgrade proběhnout korektně zejména kvůli velké variabilitě systémů používaných k instalaci.";

$bhlang['install:title:choose_old_version'] = "Vyberte starou verzi";

$bhlang['install:upgrade:choose_text'] = "Zvolte prosím verzi, ze které provádíte upgrade. Pamatujte, že výběr nesprávné verze bude mít... zajímavé následky.";

$bhlang['button:upgrade'] = "Upgrade";

$bhlang['install:title:upgrading'] = "Provádím upgrade...";

$bhlang['install:error:no_upgradefrom'] = "Nezvolil(a) jste verzi, ze které mám provést upgrade!";
$bhlang['install:error:invalid_upgradefrom'] = "Podařilo se Vám špatně vybrat verzi, ze které mám provést upgrade. Zkuste znovu.";

$bhlang['install:upgrade:complete'] = "Upgrade dokončen. Nyní můžete systém používat. Níže si přečtěte poznámky vztahující se k upgradu.";

$bhlang['install:upgrade:notes'] = "Poznámky:";

$bhlang['install:upgrade:specific:2.0.x_to_2.1.g'] = "ByteHoard 2.1 představuje velký krok vpřed proti verzi 2.0. Má mnoho nových funkcí, některé vlastnosti z verze 2.0 však byly naopak zrušeny. Mezi důležité změny patří: <ul><li>Administrační sekce je teď přístupná přes URL ByteHoardu doplněné o /administrator/ (Pamatujte, že odkaz 'Administrační sekce' by měl být také k dispozici).</li>
<li>Všichni Vaši uživatelé byli přeneseni a ti z nich, kteří byli administrátory by jimi měli zůstat. Byly však smazány všechny čekající žádosti o registraci.</li>
<li>Všechny odkazy na SouborovuPoštu by měly fungovat, dokud nevypší jejich platnost.</li>
<li>Všechny staré skupiny a jejich členové byli převedni.</li>
<li>Všechny odkazy SouborvéPošty teď standardně odchází z e-mailových adres uživatelů.</li>
<li>Pokud byly e-mailové funkce vypnuty, jsou teď znovu aktivní.</li>
<li>Uživatelské kvóty byly odstraněny, protože dosud nejsou implementovány.</li></ul><br><br>Doufáme, že si ByteHoard 2.1 užijete.";

$bhlang['label:remind_in'] = "Připomeň: ";
$bhlang['text:days_before_expiry'] = "dní před vypršením (0 = Žádné připomenutí)";

$bhlang['title:group_administration'] = "Administrace skupin";

$bhlang['explain:group_administration'] = "Zde můžete spravovat uživatelské skupiny. <br><br>Všechny existující skupiny jsou zobrazeny níže, včetně členů, které obsahují. <br>Chcete-li odtranit uživatele ze skupiny, klikněte na 'Odstraň' vedle jeho uživatelského jména. <br>Pro přidání uživatele do skupiny, vložte jeho jméno a jméno příslušné skupiny do vrchního formuláře a stiskněte 'Přidej do skupiny'. <br><br>Pamatujte prosím, ze skupiny jsou jen množinami uživatelů; chcete-li vytvořit novou skupinu, prostě do ní přidejte uživatele (např. pro vytvoření nové skupiny 'kamarádi' jednoduše přidejte uživatele do 'kamarádi').<br> Také pamatujte, že skupina jménem 'Všichni(All)' je vyhrazena pro interní použití a jména všech skupin jsou převáděna na malá písmena.";

$bhlang['button:remove'] = "Odstranit";
$bhlang['label:group'] = "Skupina: ";
$bhlang['button:add_to_group'] = "Přidat do skupiny";

$bhlang['module:admin'] = "Administrace";
$bhlang['moduledesc:admin'] = "Vstup do administrační sekce.";

$bhlang['module:return'] = "Návrat do BH";
$bhlang['moduledesc:return'] = "Návrat do hlavní sekce systému ByteHoard.";

$bhlang['module:groups'] = "Skupiny";
$bhlang['moduledesc:groups'] = "Vytváření a správa uživatelských skupin.";

$bhlang['text:no_groups'] = "V této chvíli neexistují žádné skupiny.";

$bhlang['notice:user_added_to_group'] = "Uživatel '#USERNAME#' byl přidán do skupiny '#GROUP#'.";
$bhlang['notice:user_removed_from_group'] = "Uživatel '#USERNAME#' byl odstraněn ze skupiny '#GROUP#'.";
$bhlang['error:user_is_in_group'] = "Uživatel '#USERNAME#' je již členem skupiny '#GROUP#'.";
$bhlang['error:user_does_not_exist'] = "Uživatel '#USERNAME# neexistuje!";

$bhlang['title:file_download'] = "Stažení souboru";
$bhlang['label:filesize'] = "Velikost souboru: ";
$bhlang['label:filename'] = "Název souboru: ";
$bhlang['label:from'] = "Od: ";
$bhlang['label:md5'] = "MD5 Hash: ";
$bhlang['button:download_file'] = "Stáhnout #FILENAME#";
$bhlang['error:md5_file_too_large'] = "(příliš velký soubor; MD5 nepočítán)";

$bhlang['explain:filelink_download'] = "Stažení souboru by mělo začít za několik vteřin. Pokud ne, klikněte na níže uvedený odkaz. Pokud Váš prohlížeč dokument zobrazí, místo, aby jej stáhnul, můžete na odkaz kliknout pravým tlačítkem a zvolit 'Uložit odkaz jako...' nebo 'Uložit cíl jako...' pro uložení souboru na Váš počítač.";

$bhlang['error:quota_exceeded'] = "Touto operací by jste překročil(a) svou kvótu #QUOTA#.";

$bhlang['label:settings_lang'] = "Jazykový soubor: ";
$bhlang['explain:settings_lang'] = "Soubor s definicí jazyka, který má být použit.";

$bhlang['label:quota'] = "Kvóta: ";

$bhlang['label:_mb'] = "MB";

$bhlang['explain:edit_quota'] = "(Poznámka: 0 nebo prázdné pole znamená neomezený prostor)";
$bhlang['error:quota_not_a_number'] = "Zadaná kvóta není platným číslem!";

$bhlang['title:quota'] = "Kvóta";
$bhlang['explain:you_have_used_some_quota'] = "Používáte #QUOTAUSED# ze své #QUOTA# kvóty.";

$bhlang['title:types_administration'] = "Administrace typů uživatelů/kvót";
$bhlang['error:missed_something'] = "Nezadal(a) jste hodnotu do požadovaného pole.";
$bhlang['explain:types_administration'] = "Použijte tuto stránku pro vytváření a mazání uživatelských typů (kvóty/cenové plány).";
$bhlang['column:type_name'] = "Název typu";
$bhlang['column:type_size'] = "Kvóta typu";

$bhlang['module:types'] = "Uživatelské typy";
$bhlang['moduledesc:types'] = "Editace uživatelských typů";

$bhlang['column:edit_quota'] = "Edituj kvótu (v MB)";
$bhlang['column:delete'] = "Smaž";

$bhlang['notice:type_updated'] = "Typ vytvořen/změněn.";

