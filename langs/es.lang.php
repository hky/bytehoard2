﻿<?php

/*
 * ByteHoard 2.1
 * Copyright (c) Andrew Godwin & contributors 2004
 *
 *   Language file
 *   $Id: en.lang.php,v 1.11 2005/07/28 22:38:50 andrewgodwin Exp $
 *
 */
 
#name English
#author Andrew Godwin
#description The (default) english language file.

$bhlang['module:main'] = "Página principal";
$bhlang['module:login'] = "Entrar";
$bhlang['module:logout'] = "Salir";
$bhlang['module:find'] = "Buscar archivo";

$bhlang['title:login'] = "Formulario de entrada";
$bhlang['explain:login'] = "Introduce tu usuario y contraseña para entrar en el sistema.";
$bhlang['label:username'] = "Usuario: ";
$bhlang['label:password'] = "Contraseña: ";
$bhlang['button:login'] = "Entrar";

$bhlang['title:folders'] = "carpetas";

$bhlang['title:viewing_directory'] = "Viendo carpeta:";
$bhlang['error:not_a_dir'] = "La ruta de archivo provista no es un directorio.";

$bhlang['error:nothing_in_toolbar'] = "No tiene objetos en su barra de herramientas. Se trata de un error grave; por favor, consulte a su administrador.";

$bhlang['error:not_a_file'] = "El archivo provisto no es el adecuado para esta acción.";
$bhlang['error:no_file_specified'] = "Esta acción requiere un archivo, pero ha llegado a este punto sin ninguno.";
$bhlang['title:viewing_file'] = "Archivo: ";

$bhlang['module:download'] = "Descargar";
$bhlang['moduledesc:download'] = "Descarga este archivo";

$bhlang['module:delete'] = "Borrar";
$bhlang['moduledesc:delete'] = "Borra este archivo";

$bhlang['label:_files'] = " Archivos";

$bhlang['title:error'] = "Error";
$bhlang['explain:error_occured'] = "Se ha producido un error. Ha sido registrado, pero por favor informe a su administrador si piensa que es grave.";

$bhlang['error:page_not_exist'] = "¡Esa página no existe!";
$bhlang['error:file_not_exist'] = "¡Ese archivo no existe!";
$bhlang['error:directory_no_exist'] = "¡Ese directorio no existe!";

$bhlang['title:deleting_'] = "Borrando ";
$bhlang['explain:delete'] = "¿Seguro que quieres borrar este archivo?";

$bhlang['button:delete_file'] = "Borrar archivo";
$bhlang['button:cancel'] = "Cancelar";
$bhlang['notice:file_deleted'] = "Archivo borrado.";

$bhlang['error:access_denied'] = "No tiene permiso para hacer eso.";

$bhlang['title:upload'] = "Subir archivos";

$bhlang['explain:upload'] = "Por favor, selecciona los archivos que quieres subir, y después haz click en el botón correspondiente para empezar a subirlos. <br><br>Por favor ten en cuenta que mientras los archivos se están subiendo no pasará nada en esta pantalla. Sin embargo, la barra de progreso del navegador debe mostrarte el estado del proceso.";
$bhlang['module:upload'] = "Subir archivos";
$bhlang['moduledesc:upload'] = "Subir archivos desde tu ordenador";

$bhlang['button:upload'] = "Subir";
$bhlang['label:uploading_to'] = "Subir a: ";
$bhlang['button:change_folder'] = "Cambiar";
$bhlang['title:choose_folder'] = "Elige carpeta";

$bhlang['error:no_write_permission'] = "No tienes permiso para editar ese archivo.";

$bhlang['module:url'] = "Archivo URL";
$bhlang['moduledesc:url'] = "Muestra la URL del archivo";

$bhlang['explain:the_url_to_that_file_is_'] = "La URL de ese archivo es: ";
$bhlang['notice:file_#FILE#_upload_success'] = "El archivo #FILE# ha sido subido con éxito.";

$bhlang['notice:logged_out'] = "Ya estás fuera.";

$bhlang['log:#USER#_logged_out'] = "Salida de #USER#";
$bhlang['notice:logged_in_as_#USER#'] = "Has entrado como #USER#. Bienvenido.";
$bhlang['notice:login_failed'] = "El usuario o la contraseña es incorrecta.";
$bhlang['log:failed_login_#USER#'] = "Identificación sin éxito de #USER#";
$bhlang['log:successful_login_#USER#'] = "Identificación con éxito de #USER#";
$bhlang['log:#USER#_uploaded_#FILE#'] = "Subiendo el archivo #FILE# por #USER#";

$bhlang['title:add_folder'] = "Añadir carpeta";
$bhlang['module:addfolder'] = "Añadir carpeta";
$bhlang['moduledesc:addfolder'] = "Te permite crear una carpeta";
$bhlang['explain:add_folder'] = "Estás creando una nueva carpeta. Selecciona la carpeta existente en la que te quieres crearla, y después elige el nombre de tu nueva carpeta.";
$bhlang['button:add_folder'] = "Crear carpeta";
$bhlang['label:folder_name'] = "Nombre de carpeta: ";
$bhlang['label:create_in'] = "Crear en: ";
$bhlang['log:#USER#_denied_#PAGE#'] = "Acceso denegado a #PAGE# para #USER#";

$bhlang['log:#USER#_created_#FOLDER#'] = "Carpeta #FOLDER# creada por #USER#";
$bhlang['notice:folder_created'] = "Carpeta creada.";

$bhlang['notice:file_#FILE#_upload_failure'] = "Subida sin éxito del archivo #FILE#.";
$bhlang['log:#USER#_failed_upload_#FILE#'] = "Subida sin éxito del archivo #FILE# por #USER#";

$bhlang['explain:edit'] = "Haz tus cambios al siguiente archivo y haz click en guardar para guardar esos cambios. Ten en cuenta que si no se trata de un archivo de texto (por ejemplo, si se trata de un documento de procesador de texto o una hoja de cálculo), es probable que este editor sea inservible, e intentar guardar pueda dañar su archivo.";
$bhlang['title:editing_#FILE#'] = "Editando #FILE#";
$bhlang['button:save'] = "Guardar";

$bhlang['module:edit'] = "Editar";
$bhlang['moduledesc:edit'] = "Editar el archivo (en modo texto)";

$bhlang['notice:file_saved'] = "Archivo guardado.";
$bhlang['log:#USER#_modified_#FILE#'] = "Archivo #FILE# modificado por #USER#";

$bhlang['module:htmledit'] = "Editor HTML";
$bhlang['moduledesc:htmledit'] = "Editar el archivo (en modo HTML)";

$bhlang['explain:htmledit'] = "Haz tus cambios a la página y luego haz click en guardar para guardarlos";

$bhlang['label:copy_to'] = "Copiar a: ";
$bhlang['explain:copy'] = "Selecciona dónde quieres copiar el archivo y su nuevo nombre. Luego haz click en copiar.";
$bhlang['label:new_name'] = "Nuevo nombre: ";
$bhlang['button:copy'] = "Copiar";
$bhlang['title:copying_#FILE#'] = "Copiando #FILE#";
$bhlang['log:#USER#_copied_#FILE#_to_#DEST#'] = "Archivo #FILE# copiado a #DEST# por #USER#";
$bhlang['notice:file_copied'] = "Archivo copiado.";
$bhlang['module:copy'] = "Copiar";
$bhlang['moduledesc:copy'] = "Copiar archivo con otro nombre o a otra carpeta";

$bhlang['title:uploading'] = "Subiendo";
$bhlang['explain:uploading'] = "Se está subiendo un archivo. Puede tardar un rato.<br>Esta ventana se cerrará cuando el proceso finalice.";

$bhlang['notice:you_must_be_admin'] = "Tienes que ser administrador para acceder a este área.";
$bhlang['title:__administration'] = " [Administración]";

$bhlang['title:welcome_to_administration'] = "Bienvenido a la administración de Bytehoard";
$bhlang['explain:welcome_to_administration'] = "Bienvenido al panel de administración de ByteHoard. Selecciona una sección de los enlaces situados en esta pantalla.";

$bhlang['module:users'] = "Usuarios";
$bhlang['moduledesc:users'] = "Administrar usuarios y grupos";

$bhlang['title:user_administration'] = "Administración de usuario";
$bhlang['explain:user_administration'] = "Selecciona un grupo para ver los usuarios en ese grupo. Para editar un usuario, haz click en su nombre de usuario.";

$bhlang['label:disk_space_used'] = "Espacio en disco usado";
$bhlang['label:upload_bandwidth_used'] = "Ancho de banda para subir usado";
$bhlang['label:download_bandwidth_used'] = "Ancho de banda para descargar usado";

$bhlang['title:views'] = "vistas";

$bhlang['module:sharing'] = "Compartir";
$bhlang['moduledesc:sharing'] = "Comparte esto con otros usuarios";

$bhlang['title:sharing_'] = "Compartición de ";
$bhlang['explain:sharing'] = "Aquí puedes configurar quién más puede ver este archivo, así como lo que pueden hacer con él.";
$bhlang['label:users'] = "Usuarios";
$bhlang['label:groups'] = "Grupos";
$bhlang['label:public'] = "Todos";
$bhlang['explain:no_users_sharing_to'] = "No estás compartiendo con ningún usuario.";
$bhlang['explain:no_groups_sharing_to'] = "No estás compartiendo con ningún grupo.";

$bhlang['label:sharing_hidden'] = "Oculto";
$bhlang['explain:sharing_hidden'] = "Este archivo está oculto.";

$bhlang['label:sharing_viewable'] = "Visible";
$bhlang['explain:sharing_viewable'] = "Este archivo se puede ver y copiar pero no modificar.";

$bhlang['label:sharing_writable'] = "Editable";
$bhlang['explain:sharing_writable'] = "Este archivo se puede ver, mover, modificar o eliminar.";

$bhlang['label:sharingfolder_hidden'] = "Oculta";
$bhlang['explain:sharingfolder_hidden'] = "Esta carpeta está oculta.";

$bhlang['label:sharingfolder_viewable'] = "Visible";
$bhlang['explain:sharingfolder_viewable'] = "Esta carpeta se puede ver y copiar pero no modificar.";

$bhlang['label:sharingfolder_writable'] = "Editable";
$bhlang['explain:sharingfolder_writable'] = "Esta carpeta se puede ver, mover, modificar o eliminar";

$bhlang['label:change_to'] = "Cambiar a: ";

$bhlang['notice:permissions_changed'] = "Permisos modificados";

$bhlang['label:add_user'] = "Añadir Usuario:";
$bhlang['label:add_group'] = "Añadir Grupo:";
$bhlang['button:add'] = "Añadir";

$bhlang['button:edit'] = "Editar";
$bhlang['button:delete'] = "Eliminar";

$bhlang['column:user_type'] = "Tipo";
$bhlang['column:used_space'] = "Espacio usado";
$bhlang['column:bandwidth_30_days'] = "Ancho de banda (últimos 30 días)";
$bhlang['column:actions'] = "Acciones";
$bhlang['column:username'] = "Nombre de usuario";

$bhlang['explain:delete_user'] = "¿Estás seguro de que quieres eliminar este usuario (y sus archivos) de manera irreversible?";
$bhlang['button:delete_user'] = "Eliminar usuario";
$bhlang['button:delete_user_and_files'] = "Eliminar usuario y sus archivos";

$bhlang['notice:user_deleted'] = "Usuario eliminado.";
$bhlang['notice:user_and_files_deleted'] = "Usuario y sus archivos eliminados.";

$bhlang['title:settings'] = "Configuración";
$bhlang['explain:settings'] = "Aquí puedes cambiar la configuración básica de ByteHoard. Edita las opciones que quieres modificar y luego haz click en 'Guardar configuración' para guardarlas.";

$bhlang['label:settings_usetrash'] = "¿Usar papelera de reciclaje?";
$bhlang['explain:settings_usetrash'] = "Si seleccionas que sí, los archivos no serán borrados permanentemente sino que serán movidos a la carpeta 'Trash'.";

$bhlang['label:yes'] = "sí";
$bhlang['label:no'] = "no";

$bhlang['button:save_settings'] = "Guardar configuración";

$bhlang['module:settings'] = "Configuración";
$bhlang['moduledesc:settings'] = "Cambiar configuración básica";

$bhlang['label:settings_sitename'] = "Nombre del sitio";
$bhlang['explain:settings_sitename'] = "El nombre del sitio que será mostrado en la página web y en los emails.";

$bhlang['label:settings_limitthumbs'] = "¿Desea que sólo se hagan miniaturas de los archivos pequeños?";
$bhlang['explain:settings_limitthumbs'] = "Si selecciona que no, las imágenes grandes pueden causar que el sistema se ralentice temporalmente mientras se han sus miniaturas.";

$bhlang['notice:settings_saved'] = "Configuración guardada.";

$bhlang['title:delete_user'] = "Borrar usuario";
$bhlang['title:edit_user'] = "Editar usuario";

$bhlang['title:settings'] = "Configuración";

$bhlang['explain:edit_user'] = "Haz los cambios que desees a este usuario. Luego haz click en 'Guardar usuario' para guardar dichos cambios. Si no quieres cambiar la contraseña, simplemente deja las cajas en blanco.";

$bhlang['subtitle:details'] = "Detalles";

$bhlang['title:editing_user_'] = "Editando usuario: ";
$bhlang['label:email'] = "Email:";
$bhlang['label:full_name'] = "Nombre completo:";
$bhlang['label:user_type'] = "Tipo de usuario:";

$bhlang['value:guest'] = "Invitado";
$bhlang['value:normal'] = "Normal";
$bhlang['value:admin'] = "Administrador";

$bhlang['subtitle:password'] = "Contraseña";
$bhlang['label:new_password'] = "Nueva contraseña:";
$bhlang['label:repeat_new_password'] = "Repite la nueva contraseña:";

$bhlang['button:save_user'] = "Guardar usuario";

$bhlang['error:passwords_dont_match'] = "¡Las contraseñas no coinciden!";
$bhlang['notice:user_updated'] = "¡Usuario actualizado!";

$bhlang['title:signup'] = "Registrarse";
$bhlang['explain:signup'] = "Rellena los campos que se muestran a continuación y presiona 'Registrarse' para registrarte en el sistema.";

$bhlang['label:repeat_password'] = "Repite la contraseña:";
$bhlang['button:signup'] = "Registrarse";

$bhlang['module:signup'] = "Registrar";
$bhlang['moduledesc:signup'] = "Registrar una cuenta de usuario";

$bhlang['error:username_in_use'] = "Ese nombre de usuario ya ha sido usado. Prueba otro.";
$bhlang['notice:signup_successful_can_login'] = "¡Completado! Ahora puedes entrar con tu nuevo nombre de usuario y contraseña.";
$bhlang['log:user_signed_up_'] = "El usuario se ha registrado satisfactoriamente";
$bhlang['error:password_empty'] = "¡No has introducido una contraseña!";

$bhlang['notice:file_description_saved'] = "La descripción del archivo ha sido guardada.";
$bhlang['title:editing_description_#FILE#'] = "Editando la descripción de #FILE#";
$bhlang['explain:editdesc'] = "Edita la descripción del archivo. Déjalo en blanco para una descripción automática.";

$bhlang['button:savedesc'] = "Guardar descripción";
$bhlang['module:editdesc'] = "Editar descripción";
$bhlang['moduledesc:editdesc'] = "Cambiar la descripción de este archivo";

$bhlang['title:appearance']  = "Presentación";
$bhlang['explain:appearance'] = "Aquí puedes cambiar la apariencia de ByteHoard eligiendo entre las diferentes plantillas que has instalado previamente. Elige la que quieras usar haciendo click en el botón 'Usa esta plantilla'. La plantilla que estés usando actualmente tiene la etiqueta 'Plantilla actual' y no tiene dicha opción. Recuerda que el panel de administración siempre es el mismo, por tanto los cambios sólo se reflejarán en el sistema principal.";

$bhlang['label:author'] = "Autor: ";

$bhlang['module:appearance'] = "Presentación";
$bhlang['moduledesc:appearance'] = "Edita la apariencia del sistema.";

$bhlang['explain:current_skin'] = "Plantilla actual";

$bhlang['button:use_this_skin'] = "Usa esta plantilla";
$bhlang['notice:skin_changed'] = "Plantilla en uso modificada.";

$bhlang['label:settings_signupmoderation'] = "¿Desea moderar el registro de nuevos usuarios?";
$bhlang['explain:settings_signupmoderation'] = "Si elige 'sí', el administrador deberá aprobar el registro de todos los nuevos usuarios.";

$bhlang['error:validation_link_wrong'] = "¡Error! El enlace de validación que has elegido ya no es válido. Puede que haya caducado. Prueba a registrarte de nuevo.";
$bhlang['log:user_validated_'] = "El usuario #USER# ha validado su nombre de usuario";
$bhlang['log:user_signup_m_pending_'] = "El usuario #USER# está pendiente de moderación";

$bhlang['notice:moderation_now_pending'] = "Tu petición de registro ha sido validada satisfactoriamente y ahora se ha enviado al administrador para su aprobación. Serás notificado cuando tu petición haya sido aceptada o denegada.";
$bhlang['notice:do_email_validation'] = "Se ha mandado un email a la dirección de email que pusiste con detalles de cómo verificar que dicha cuenta y seguir con el proceso de registro.";

$bhlang['emailsubject:registration_validation'] = "Verificación de cuenta de correo para su registro en #SITENAME#";


# Note to translators: Just the stuff inside the double quotes, it's allowed to go over multiple lines.
# Another note: Lines beginning with a hash (#) are ignored. So this line is ignored. And the one above.
$bhlang['email:registration_validation'] = "Tu petición de registro está esperando la verificación de tu cuenta de correo.

Para verificarla, visita la siguiente dirección en tu navegador web:

#LINK#

Si no verificas tu cuenta de correo en 7 días, tu registro de usuario será borrado.";


$bhlang['error:email_error'] = "Actualmente tenemos problemas con el sistema de email, por lo que tu petición no puede ser completada en este momento. Inténtalo más tarde.";

$bhlang['notice:validation_already_done_pending_approval'] = "Ya habías validado tu cuenta. Actualmente se encuentra esperando la aprobación del administrador.";

$bhlang['title:registrations_administration'] = "Administración de registros";
$bhlang['module:registrations'] = "Registros";
$bhlang['moduledesc:registrations'] = "Acepta o deniega registros de nuevos usuarios";
$bhlang['explain:registration_administration'] = "Esta es la lista de los registros pendientes de aprobación. A continuación se muestran los detalles de cada usuario junto con las opciones de aceptación o denegación de su petición. Elige correctamente, ya que no hay confirmación de la acción que tomes.";

$bhlang['notice:registration_moderation_off'] = "La moderación de nuevos registros está desactivada. Todos los nuevos usuarios son aprobados automáticamente después de ser verificados. Si deseas activar esta característica, vete a configuración y cambia la opción '¿Desea moderar el registro de nuevos usuarios?' a 'sí'.";

$bhlang['button:reject'] = "Denegar";
$bhlang['button:accept'] = "Aceptar";

$bhlang['notice:#USER#_accepted'] = "El usuario #USER# ha sido aceptado.";
$bhlang['notice:#USER#_rejected'] = "El usuario #USER# ha sido denegado.";

$bhlang['emailsubject:registration_accepted'] = "Tu registro ha sido aceptado en #SITENAME#";
$bhlang['email:registration_accepted'] = "Tu registro ha sido aceptado por un administrador.

Ahora ya puedes entrar con los siguientes datos:
Usuario: #USERNAME#
Contraseña: La que elegiste en el registro

Espero que disfrutes del nuevo servicio.";

$bhlang['emailsubject:registration_rejected'] = "Tu registro ha sido denegador en #SITENAME#";
$bhlang['email:registration_rejected'] = "Tu registro no ha sido aprobado. Tu cuenta de usuario no será creada.

Si crees que es un error del sistema puedes intentar ponerte en contacto con el administrador.";


$bhlang['error:registration_doesnt_exist'] = "Ese registro ya no existe. Puede que haya sido aprobado o denegado por otro administrador.";

$bhlang['error:username_too_long'] = "Ese nombre de usuario es demasiado largo. Debe de tener menos de 255 caracteres.";

$bhlang['button:go'] = "Enviar";
$bhlang['button:request_reset'] = "Petición de reseteo";

$bhlang['title:recover_password'] = "Recuperar contraseña";
$bhlang['explain:recover_password'] = "Entra tu nombre de usuario o contraseña y haz click en 'Enviar' para pedir la recuperación de tu contraseña. A continuación recibirás un email con tu nombre de usuario y un enlace para resetear tu contraseña.";

$bhlang['text:or'] = "o";

$bhlang['module:passreset'] = "¿Has perdido tu usuario o contraseña?";
$bhlang['moduledesc:passreset'] = "Recupera tu usuario o restaura tu contraseña";

$bhlang['emailsubject:passreset_request'] = "Petición de reseteo de la contraseña en #SITENAME#";
$bhlang['email:passreset_request'] = "Usted, o alguien haciéndose pasar por usted, ha pedido que se resetee su contraseña. Para resetear tu contraseña visita el enlace siguiente en su navegador:

#LINK#

Si no has pedido resetear su contraseña, ignora este email y bórralo. Las peticiones de reseteo son borradas en 48 horas.";

$bhlang['error:username_doesnt_exist'] = "¡Error! Ese usuario no existe en nuestra base de datos.";
$bhlang['error:email_doesnt_exist'] = "¡Error! Ese email no existe en nuestra base de datos.";

$bhlang['emailsubject:passreset_u_request'] = "Petición de usuario y contraseña en #SITENAME#";
$bhlang['email:passreset_u_request'] = "Usted, o alguien haciéndose pasar por usted, ha pedido que se le recuerde su usuario.

Usuario: #USERNAME#

Para resetear tu contraseña visita el enlace siguiente en su navegador:

#LINK#

Si no has pedido resetear su contraseña, ignora este email y bórralo. Las peticiones de reseteo son borradas en 48 horas.";

$bhlang['notice:passreset_request_sent'] = "Se le ha mandado un email con las instrucciones siguientes.";

$bhlang['error:passreset_link_invalid'] = "El enlace no es válido: ya ha caducado o lo has copiado mal.";

$bhlang['emailsubject:passreset_new_password'] = "Tu nueva contraseña para #SITENAME#";
$bhlang['email:passreset_new_password'] = "Tu nueva contraseña para el sitio es:

#PASSWORD#

Esta contraseña ha sido generada aleatoriamente y puede ser cambiada cuando entres en el sitio.";

$bhlang['notice:passreset_new_password_sent'] = "Tu nueva contraseña ha sido enviada a tu cuenta de correo electrónico.";

$bhlang['error:username_invalid'] = "Ese usuario no es válido.";

$bhlang['error:systemwrong'] = "Error fatal: la configuración de su sistema no permite que funcione.";

$bhlang['title:options'] = "Opciones";
$bhlang['module:options'] = "Opciones";
$bhlang['moduledesc:options'] = "Cambia tus opciones";
$bhlang['explain:options'] = "Aquí puedes cambiar varias opciones sobre el funcionamiento y apariencia del sistema, así como cambiar tus detalles de contacto y tu contraseña.";
$bhlang['title:change_password'] = "Cambiar contraseña";
$bhlang['title:interface_options'] = "Opciones de la interfaz";
$bhlang['title:profile'] = "Perfil";

$bhlang['label:old_password'] = "Contraseña antigua: ";
$bhlang['button:change_password'] = "Cambiar contraseña";
$bhlang['error:old_password_invalid'] = "¡Tu contraseña antigua es errónea! Intenta escribirla de nuevo.";
$bhlang['notice:password_changed'] = "La contraseña ha sido modificada satisfactoriamente.";

$bhlang['error:unknown'] = "Ha ocurrido un error desconocido. Notifíquelo al administrador.";
$bhlang['warning:blank_password'] = "Estás usando una contraseña en blanco. Eso no es una buena idea.";

# These are all options! We're not going to collect all this inforation about every user.
# The administrator will be able to choose which ones to allow.
$bhlang['profile:email'] = "Email";
$bhlang['profile:fullname'] = "Nombre completo";
$bhlang['profile:website'] = "Página web";
$bhlang['profile:telephone'] = "Teléfono";
$bhlang['profile:fax'] = "Fax";
$bhlang['profile:jabber'] = "Jabber";
$bhlang['profile:icq'] = "ICQ";
$bhlang['profile:msn'] = "MSN Messenger";
$bhlang['profile:aim'] = "AIM";
$bhlang['profile:yahoo'] = "Yahoo! Messenger";
$bhlang['profile:nickname'] = "Apodo";
$bhlang['profile:position'] = "Posición";
$bhlang['profile:location'] = "Lugar";
$bhlang['profile:gender'] = "Sexo";
$bhlang['profile:address'] = "Dirección";
$bhlang['profile:postcode'] = "Código postal";
$bhlang['profile:zipcode'] = "Zipcode"; //TODO
$bhlang['profile:nationality'] = "Nacionalidad";
$bhlang['profile:latitude'] = "Latitud";
$bhlang['profile:longitude'] = "Longitud";
$bhlang['profile:occupation'] = "Trabajo";
$bhlang['profile:status'] = "Estatus";

$bhlang['button:save_profile'] = "Guardar perfil";
$bhlang['notice:profile_saved'] = "Perfil guardado.";

$bhlang['error:cannot_determine_update_server'] = "El servidor de actualización no pudo ser localizado. Posiblemente el sistema esté caído. Inténtelo más tarde.";
$bhlang['error:cannot_download_package'] = "El paquete no puede ser descargado desde el servidor remoto.";
$bhlang['error:system_setup_wrong'] = "Tu sistema no puede descargar los archivos necesarios. Debes hacerlo manualmente.";
$bhlang['error:cannot_write_to_package'] = "No puedo escribir en el archivo. Comprueba que los permisos del directorio de ByteHoard son correctos.";

$bhlang['title:folder_files'] = "Archivos";
$bhlang['title:folder_actions'] = "Opciones de carpeta";
$bhlang['module:deletefolder'] = "Borrar";
$bhlang['moduledesc:deletefolder'] = "Borrar esta carpeta y archivos que contiene";
$bhlang['module:copyfolder'] = "Copiar";
$bhlang['moduledesc:copyfolder'] = "Copiar esta carpeta a otro lugar";
$bhlang['module:sharingfolder'] = "Compartir";
$bhlang['moduledesc:sharingfolder'] = "Compartir esta carpeta con otros usuarios";
$bhlang['notice:folder_deleted'] = "Carpeta borrada";

$bhlang['error:cannot_delete_that'] = "No puedes borrar ese archivo o carpeta.";

$bhlang['button:delete_folder'] = "Borrar carpeta";
$bhlang['explain:deletefolder'] = "¿Quieres borrar esta carpeta y todos su contenido?";

$bhlang['label:settings_fromemail'] = "Dirección 'De:' del sistema: ";
$bhlang['explain:settings_fromemail'] = "La dirección de la que parece que provienen todos los emails del sistema.";
$bhlang['label:settings_imageprog'] = "Programa de imagen: ";
$bhlang['explain:settings_imageprog'] = "Tiene que ser 'imagemagick' o 'gd'.";
$bhlang['label:settings_syspath_convert'] = "Ruta de 'convert': ";

# Translators; Leave $"."PATH as it is.
$bhlang['explain:settings_syspath_convert'] = "Sólo es necesario si has seleccionado 'imagemagick'. Puede usar 'convert' si el programa está en $"."PATH.";

$bhlang['label:settings_fileroot'] = "Directorio del sistema de ficheros virtual: ";
$bhlang['explain:settings_fileroot'] = "El directorio donde los archivos de ByteHoard están almacenados. NO CAMBIAR, a no ser que sepas lo que haces. La ruta NO DEBE terminar en una barra.";

$bhlang['error:no_gd'] = "¡Error! GD no está instalado y lo has seleccionado como programa de imagen. Cámbialo.";

$bhlang['explain:sharingfolder'] = "Aquí puedes configurar quien más puede ver y modificar esta carpeta. Ten en cuenta que todos los cambios que hagas en esta carpeta afectarán a todos los archivos y carpetas de su interior. Por esta razón no te recomendamos que compartas tu carpeta principal, sino que crees una subcarpeta para compartir.";

$bhlang['label:sharingfolder_owner'] = "Propietario";
$bhlang['explain:sharingfolder_owner'] = "Tiene el control total sobre la carpeta.";
$bhlang['label:sharing_owner'] = "Propietario";
$bhlang['explain:sharing_owner'] = "Tiene el control total sobre el archivo.";

$bhlang['error:permissions_self'] = "No puedes cambiar tus permisos.";
$bhlang['notice:permissions_changed'] = "Permisos cambiados.";
$bhlang['notice:permissions_group_added'] = "Grupo agregado.";
$bhlang['notice:permissions_user_added'] = "Usuario agregado.";
$bhlang['notice:permissions_group_deleted'] = "Grupo borrado.";
$bhlang['notice:permissions_user_deleted'] = "Usuario borrado.";

$bhlang['label:delete_user'] = "Borrar usuario: ";
$bhlang['label:delete_group'] = "Borrar grupo: ";

$bhlang['button:return'] = "Volver";

$bhlang['module:returntofolder'] = "Volver a la carpeta";
$bhlang['moduledesc:returntofolder'] = "Ver la carpeta en la cual se encuentra el archivo";

$bhlang['notice:folder_copied'] = "Carpeta copiada.";

$bhlang['module:filelink'] = "Archivo por correo electrónico";
$bhlang['moduledesc:filelink'] = "Mandar por email o crear un enlace temporal a este archivo";
$bhlang['error:no_filepath'] = "Error fatal: no se ha especificado ruta de archivo.";

$bhlang['title:filemail'] = "Archivo por correo";
$bhlang['explain:filemail'] = "'Archivo por correo' te permite enviar por email un enlace temporal a este archivo que caducará después de un cierto periodo de tiempo. Completa los siguientes campos con el receptor del correo, el asunto y un mensaje opcional. Este enlace será añadido al final del mensaje. Para especificar varios receptores, deberás separarlos por coma.";

$bhlang['label:subject'] = "Asunto: ";
$bhlang['label:message'] = "Mensaje: ";

$bhlang['error:no_emailaddr'] = "No has especificado una dirección de correo electrónico.";
$bhlang['error:no_emailsubj'] = "No has especificado un asunto para el correo electrónico.";
$bhlang['error:invalid_email_#EMAIL#'] = "La dirección de correo #EMAIL# no es válida.";

$bhlang['notice:email_sent'] = "El correo se ha enviado satisfactoriamente.";
$bhlang['notice:email_sent_to_#EMAIL#'] = "El correo se ha enviado satisfactoriamente a #EMAIL#.";

$bhlang['text:days'] = " días";
$bhlang['label:expires_in'] = "Caducará en: ";

$bhlang['text:max_#NUM#_days'] = "(máximo #NUM# días)";

$bhlang['error:expires_invalid'] = "Este tiempo de caducidad no es válido. Tiene que ser un número positivo.";
$bhlang['error:expires_too_much'] = "Ese tiempo de caducidad es superior al límite máximo.";

$bhlang['label:settings_maxexpires'] = "Límite máximo de caducidad en días para 'Archivo por correo': ";
$bhlang['explain:settings_maxexpires'] = "El número máximo de días que los enlaces de 'Archivo por correo' pueden ser válidos.";

$bhlang['error:no_filecode'] = "No has especificado un enlace con un código de archivo.";
$bhlang['error:filecode_invalid'] = "El enlace introducido no es válido o ya ha caducado.";

$bhlang['title:filelinks'] = "Administración de enlaces de archivo";
$bhlang['module:filelinks'] = "Archivo por correo";
$bhlang['moduledesc:filelinks'] = "Administración de enlaces de archivo";
$bhlang['explain:filelinks'] = "En esta página puede encontrar un resumen de los enlaces de 'Archivo por correo'. Están ordenados por nombre de usuario y debajo de cada hay una lista con sus enlaces, junto con el email, el archivo y la fecha en la que caduca. Puedes ver un enlace haciendo click en 'Enlace'. Puedes desactivar el enlace haciendo click en 'Borrar'.";

$bhlang['column:expires_in'] = "Caduca en";
$bhlang['column:email'] = "Correo";
$bhlang['column:file'] = "Archivo";

$bhlang['button:link'] = "Enlace";

$bhlang['notice:filelink_deleted'] = "Archivo por correo borrado.";

$bhlang['log:filelink_denied'] = "Acceso denegado a #FILELINK#";
$bhlang['log:filelink_accessed'] = "#FILEPATH# accedido por el enlace [#FILELINK#].";

$bhlang['label:settings_authmodule'] = "Módulo de autentificación: ";
$bhlang['explain:settings_authmodule'] = "Es el módulo usado para autentificar usuarios. Déjalo como el valor por defecto 'bytehoard.inc.php' si no necesitas autentificarse contra alguna otra aplicación.";

$bhlang['button:send'] = "Enviar";
$bhlang['button:back'] = "Atrás";


$bhlang['label:settings_baseuri'] = "Dirección web de la aplicación (opcional): ";
$bhlang['explain:settings_baseuri'] = "Es la dirección web del directorio principal de la aplicación. Debe de terminar en una barra. Este parámetro es opcional, por lo que puede dejarlo en blanco y se detectará automáticamente.";

$bhlang['title:overwriting_'] = "Sobreescribiendo ";
$bhlang['explain:overwrite'] = "El archivo que has subido tiene el mismo nombre de otro que ya existe. ¿Qué desea hacer?";

$bhlang['label:linkonly'] = "Crear sólo un enlace sin enviarlo por correo: ";

$bhlang['text:link__expire_in_#EXPIRE#'] = "Enlace (Caduca en #EXPIRE# días): ";

$bhlang['label:html'] = "HTML: ";
$bhlang['label:bbcode'] = "BBcode: ";
$bhlang['title:image_tags'] = "Tags de imagen";

$bhlang['error:email_empty'] = "No has especificado una dirección de correo.";

$bhlang['email:filemail_footer'] = "---------------------------------------------
El archivo estará disponible para descargarse hasta #DATE#

Archivo: #FILENAME#, #FILESIZE# (MD5: #MD5#)
#LINK#

Has recibido un enlace de un archivo desde #SYSTEMNAME#. Para descargarlo, haz click en el enlace.";

$bhlang['label:downloadnotice'] = "Enviar correos de notificación de descarga: ";

$bhlang['emailsubject:filemail_link_accessed'] = "Notificaciones de archivo por correo (#FILENAME#)";

$bhlang['email:filemail_link_accessed'] = "Esto es una notificación automática.

Se ha accedido a uno de tus enlaces de archivos por correo. Tú especificaste que querías una notificación de cualquier acceso a este archivo.

El archivo (#FILEPATH#) ha sido accedido a las #TIME#, desde la dirección IP #IP#. El correo al que enviaste este archivo es #EMAIL#.

Nota: Este enlace caduca en #EXPIRES#.";




$bhlang['install:title:bytehoard_installation'] = "Instalación de ByteHoard";

$bhlang['install:title:page_not_found'] = "Página no encontrada";
$bhlang['install:error:page_not_found'] = "¡Esta página no existe!";
$bhlang['install:title:menu'] = "Menú";
$bhlang['install:menu:install'] = "Instalar ByteHoard";
$bhlang['install:menu:upgrade'] = "Actualizar ByteHoard";
$bhlang['install:menu:documentation'] = "Documentación";
$bhlang['install:menu:systeminfo'] = "Información del sistema";

$bhlang['install:text:install_intro'] = "Bienvenido a ByteHoard ".$bhconfig['version'].". A continuación vas a ejecutar el proceso de instalación. Este proceso es relativamente sencillo. El sistema intentará ayudarle en la medida de lo posible. Necesitará tener a mano los detalles de configuración de su base de datos, así como asegurarse de que el sistema cumple los requisitos mínimos necesarios para instalar ByteHoard. Si prosigue con la instalación, deberá aceptar la licencia de esta aplicación: la GNU GPL v2. Puede encontrar una copia de dicha licencia en el archivo LICENSE o en <a href='http://www.gnu.org/copyleft/gpl.html'>http://www.gnu.org/copyleft/gpl.html</a>. Para soporte técnico, actualizaciones y noticias sobre el desarrollo puede visitar la web de <a href='http://bytehoard.org'>ByteHoard</a>.";

$bhlang['install:title:introduction'] = "Introducción";
$bhlang['install:title:systemchecks'] = "Comprobación del sistema";
$bhlang['install:text:checking_system'] = "Comprobando el sistema ...";
$bhlang['install:label:php'] = "PHP";
$bhlang['install:label:extensions'] = "Extensiones";
$bhlang['install:label:external_progs'] = "Programas externos";
$bhlang['install:label:filesystem'] = "Sistema de archivos";
$bhlang['install:check:php'] = "Versión de PHP: ";
$bhlang['install:check:safe_mode'] = "Safe_mode está desactivado: ";
$bhlang['install:check:pcre'] = "PCRE: ";
$bhlang['install:check:gd'] = "GD: ";
$bhlang['install:check:imagemagick'] = "ImageMagick: ";

$bhlang['install:check:php5'] = "PHP5";
$bhlang['install:check:failed'] = "Fallido";
$bhlang['install:check:failedoption'] = "Fallido (Opcional)";
$bhlang['install:check:ok'] = "OK";

$bhlang['install:check:failedphp'] = "Fallido (PHP 4.2 o mayor requerido)";
$bhlang['install:check:failedsafemode'] = "Fallido (safe_mode está activo)";
$bhlang['install:check:failedperm'] = "Fallido (no tengo permisos de escritura sobre el archivo)";

$bhlang['install:check:config.inc.php'] = "config.inc.php tiene permisos de escritura: ";
$bhlang['install:check:cache'] = "cache/ tiene permisos de escritura: ";

$bhlang['install:text:checksok'] = "Se han pasado todas las comprobaciones satisfactoriamente.";
$bhlang['install:text:checkswarnings'] = "Parece que tienes algunas advertencias, pero nada realmente importante. Puedes continuar o puedes intentar instalar los componentes no encontrados. Si necesitas ayuda con esto, puedes visitar la web de <a href='http://bytehoard.org'>ByteHoard</a>.";
$bhlang['install:text:checksfatals'] = "Una o varias pruebas esenciales han fallado. Debes solucionarlo antes de continuar. Si necesitas ayuda con alguno de estos problemas, puedes visitar la web de <a href='http://bytehoard.org'>ByteHoard</a>.";
$bhlang['install:text:thumbnails_disabled'] = "Nota: las miniaturas están desactivadas. Debes instalar GD o ImageMagick si quieres activarlas.";
$bhlang['button:next'] = "Siguiente";

$bhlang['install:title:choose_database'] = "Elige la base de datos";
$bhlang['install:title:configure_database'] = "Configura la base de datos";

$bhlang['install:title:system_information'] = "Información del sistema";

$bhlang['install:configdb:intro'] = "Ahora debe de especificar la conexión a la base de datos. Si no estás seguro de lo que es, habla primero con el administrador del sistema.";
$bhlang['install:configdb:nont_needed'] = "Este módulo de base de datos no necesita configuración. Puedes seguir con el próximo paso.";
$bhlang['install:title:save_database_configuration'] = "Guardando la configuración de la base de datos ...";
$bhlang['install:writeconfig:saved'] = "La configuración de la base de datos ha sido guardada satisfactoriamente.";
$bhlang['install:writeconfig:not_saved'] = "Ha habido un error mientras se guardaba la configuración de la base de datos. Por favor, comprueba los permisos del directorio de ByteHoard.";
$bhlang['install:title:init_database'] = "Inicializando base de datos";

$bhlang['install:title:test_database_configuration'] = "Probando la configuración de la base de datos ...";
$bhlang['install:testdb:ok'] = "La configuración parece ser válida.";
$bhlang['install:testdb:failed'] = "¡No se puede conectar con la base de datos! El error encontrado es el siguiente:";
$bhlang['error:error_creating_table'] = "Error creando la tabla: ";
$bhlang['install:initdb:database_initialised'] = "Base de datos inicializada.";

$bhlang['install:title:set_paths'] = "Rutas";

$bhlang['install:paths:intro'] = "Ahora debe especificar las rutas para ByteHoard. Deberás especificar la URL del sistema y el directorio donde los archivos de los usuarios serán guardados. El instalador ha intentado averiguar la URL, sin embargo, debes comprobarla ya que a veces aparecen partes extra o faltan barras. La URL debe tener al principio 'http://' o 'https://' y al final una barra '/'. El directorio de almacenamiento de archivos por defecto es un subdirectorio de ByteHoard llamado 'filestorage'. Esto no es seguro y si puede, debe crear un directorio que esté fuera de los directorios accesibles desde la web, darle los permisos adecuardos y especificar dicho directorio en su lugar. Si especifica una ruta relativa, esta será interpretada desde el directorio raíz de ByteHoard.";

$bhlang['install:paths:system_url'] = "URL del sistema: ";
$bhlang['install:paths:file_storage_directory'] = "Directorio de almacenamiento de archivos: ";

$bhlang['value:enabled'] = "No";
$bhlang['value:disabled'] = "Si (Los usuarios no pueden entrar)";

$bhlang['label:disabled'] = "¿Usuario desactivado?: ";

$bhlang['install:title:save_paths'] = "Guardando rutas ...";

$bhlang['install:savepaths:message'] = "Rutas guardadas.";

$bhlang['install:savepaths:error_baseuri'] = "Ha especificado una URL del sistema no válida. Por favor, compruebe que la URL empieza por 'http://' o 'https://' y terminar por una barra '/'.";
$bhlang['install:savepaths:error_fileroot'] = "Ha especificado una ruta de directorio no válida.";

$bhlang['install:title:create_administrator'] = "Crear administrador";

$bhlang['notice:login_failed_disabled'] = "Entrada fallida: su cuenta de usuario ha sido desactivada.";

$bhlang['log:failed_login_disabled_#USER#'] = "Se ha denegado el acceso a #USER# ya que está desacticado.";

$bhlang['title:add_user'] = "Añadir usuario";
$bhlang['explain:add_user'] = "Usa este formulario para añadir un nuevo usuario. Debe de introducir obligatoriamente un nombre de usuario, una contraseña y un correo. Los demás campos son opcionales.";
$bhlang['button:add_user'] = "Añadir usuario";
$bhlang['notice:user_added'] = "Usuario añadido.";
$bhlang['label:homedir'] = "Directorio principal: ";
$bhlang['value:normal_homedir'] = "Subdirectorio del raíz (por defecto)";
$bhlang['value:root_homedir'] = "Raíz (sin directorio principal)";
$bhlang['label:groups'] = "Grupos iniciales (separar por comas): ";
$bhlang['module:adduser'] = "Añadir usuario";
$bhlang['moduledesc:adduser'] = "Añadir un nuevo usuario al sistema.";

$bhlang['install:createadmin:explain'] = "Se ha creado un usuario administrador. Ten cuidado con estos datos. Ahora puedes cambiar la contraseña o añadir nuevos usuarios administradores cuando entres en el sistema.";


$bhlang['install:finish:explain'] = "La instalación se ha completado. Ahora puedes entrar usando el usuario de administración y la contraseña que se muestra. Ten en cuenta que DEBES de borrar completamente el directorio 'install/' antes de empezar a usar ByteHoard. Cualquier persona que tenga acceso a dicho directorio puede resetear el sistema con un nuevo usuario de administración y contraseña, así como acceder a la base de datos de ByteHoard. Dejar este directorio es un riesgo alto de seguridad. Si desea en el futuro reinstalar o actualizar ByteHoard, puede extraer dicho directorio de los archivos originales. Te recomendamos que lo primero que hagas sea entrar en el panel de administración y cambiar las opciones del sistema para que se adapte a tus necesidades. La dirección de la página principal y del panel de administración se muestran a continuación.";
$bhlang['install:finish:label:url'] = "Página principal: ";
$bhlang['install:finish:label:adminurl'] = "Panel de administración: ";

$bhlang['error:signup_disabled'] = "Los registros están deshabilitados.";
$bhlang['label:settings_signupdisabled'] = "¿Deshabilitar registros?: ";
$bhlang['explain:settings_signupdisabled'] = "Si activa esta casilla estará desactivando los registros.";

$bhlang['install:title:complete'] = "Completado";

$bhlang['install:title:bytehoard_upgrade'] = "Actualización de ByteHoard";

$bhlang['install:update:intro'] = "Bienvenido al actualizado de ByteHoard. Este programa actualizará su antigua versión de ByteHoard. Antes de seguir, realiza una copia de seguridad de los archivos antiguos de ByteHoard y de la base de datos por si la actualización no se realiza satisfactoriamente. Ten en cuenta que antes de actualizar deberás haber movido o borrado el contenido del directorio de ByteHoard excepto el archivo 'config.inc.php' y el directorio de almacenamiento de archivos de usuario. Luego tienes que haber copiado los archivos de la nueva versión de ByteHoard en el mismo directorio. Posiblemente hayas tenido que moverlos del directorio de extracción, llamado algo así como bytehoard-".str_replace(" ","-",strtolower($bhconfig['version'])).".";

$bhlang['install:title:choose_old_version'] = "Elige la versión antigua";

$bhlang['install:upgrade:choose_text'] = "Selecciona la versión de ByteHoard que está instalada. Ten en cuenta que elegir la versión equivocada tendrá resultados inesperados.";

$bhlang['button:upgrade'] = "Actualizar";

$bhlang['install:title:upgrading'] = "Actualizando ...";

$bhlang['install:error:no_upgradefrom'] = "¡No has seleccionado una versión antigua!";
$bhlang['install:error:invalid_upgradefrom'] = "Has introducido una versión antigua equivocada. Inténtalo de nuevo.";

$bhlang['install:upgrade:complete'] = "Actualización completa. Ahora puedes usar el sistema.";

$bhlang['install:upgrade:notes'] = "Notas:";

$bhlang['install:upgrade:specific:2.0.x_to_2.1.g'] = "ByteHoard 2.1 es un gran paso desde la versión 2.0. Hay muchas novedades y algunas desapariciones en esta nueva versión. Algunas de las cosas añadidas:
<ul>
<li>The administration area is now accessed by going to the ByteHoard URL followed by /administrator/ (Note that an 'administration centre' link should appear anyway).</li>
<li>All your users have been transferred over, and your old administrators remain as administrators. However, any pending registrations have been removed.</li>
<li>All your old FileMail links will continue to work until they expire.</li>
<li>All your old groups and their users have been carried over.</li>
<li>All FileMails now come from the users' email addresses by default.</li>
<li>If email functions were turned off they have been turned back on.</li>
<li>User space quotas have been removed, since this is not yet implemented.</li>
</ul>
<br><br>We hope you enjoy ByteHoard 2.1.";

$bhlang['label:remind_in'] = "Recordatorio: ";
$bhlang['text:days_before_expiry'] = "días antes de que caduque (0 = sin recordatorio)";

$bhlang['title:group_administration'] = "Administración de grupos";

$bhlang['explain:group_administration'] = "Here you can manage groups of users. <br><br>All current groups are listed below, with the users they contain. <br>To remove a user from a group, click 'Remove' next to their username. <br>To add a user to a group, enter their name and the group name in the form at the top and press 'Add'. <br><br>Please note that groups are simply collections of users; to create a new group, simply add a user to it (i.e. to create the new group 'friends' simply add a user to 'friends').<br> Also note that the group name 'All' is reserved for internal use, and that all group names will be converted to lowercase.";

$bhlang['button:remove'] = "Quitar";
$bhlang['label:group'] = "Grupo: ";
$bhlang['button:add_to_group'] = "Añadir al grupo";

$bhlang['module:admin'] = "Administración";
$bhlang['moduledesc:admin'] = "Te lleva al panel de administración.";

$bhlang['module:return'] = "Volver";
$bhlang['moduledesc:return'] = "Vuelve al sistema principal.";

$bhlang['module:groups'] = "Grupos";
$bhlang['moduledesc:groups'] = "Crear y gestionar grupos.";

$bhlang['text:no_groups'] = "Actualmente no hay grupos.";

$bhlang['notice:user_added_to_group'] = "El usuario '#USERNAME#' ha sido añadido al grupo '#GROUP#'.";
$bhlang['notice:user_removed_from_group'] = "El usuario '#USERNAME#' ha sido eliminado del grupo '#GROUP#'.";
$bhlang['error:user_is_in_group'] = "El usuario '#USERNAME#' ya está en el grupo '#GROUP#'.";
$bhlang['error:user_does_not_exist'] = "¡El usuario '#USERNAME# no existe!";

$bhlang['title:file_download'] = "Descarga de archivos";
$bhlang['label:filesize'] = "Tamaño: ";
$bhlang['label:filename'] = "Nombre: ";
$bhlang['label:from'] = "De: ";
$bhlang['label:md5'] = "Hash MD5: ";
$bhlang['button:download_file'] = "Descargar #FILENAME#";
$bhlang['error:md5_file_too_large'] = "(el MD5 no se ha calculado porque el archivo es demasiado grande)";

$bhlang['explain:filelink_download'] = "El archivo empezará a descargarse en breves instantes. Si no lo hace, haz click sobre el enlace. Si tu navegador abre un documento en lugar de guardarlo, puedes hacer click con el botón derecho sobre el enlace y elegir 'Guardar enlace como ...' para guardar el archivo en tu ordenador.";

$bhlang['error:quota_exceeded'] = "Si haces eso excederás tu cuota de #QUOTA#.";

$bhlang['label:settings_lang'] = "Archivo de idioma: ";
$bhlang['explain:settings_lang'] = "El archivo de idiomas que se usará.";

$bhlang['label:quota'] = "Cuota: ";

$bhlang['label:_mb'] = "MB";

$bhlang['explain:edit_quota'] = "(Nota: 0 o blanco significa espacio ilimitado)";
$bhlang['error:quota_not_a_number'] = "¡La cuota que has introducido no es un número válido!";

$bhlang['title:quota'] = "Cuota";
$bhlang['explain:you_have_used_some_quota'] = "Has usado #QUOTAUSED# de #QUOTA# de tu cuota.";

$bhlang['title:types_administration'] = "Tipos de usuario/administración de cuotas";
$bhlang['error:missed_something'] = "No has introducido un valor para un campo obligatorio.";
$bhlang['explain:types_administration'] = "Usa esta pantalla para crear y borrar tipos de usuario, cuotas y planes de precios.";
$bhlang['column:type_name'] = "Nombre";
$bhlang['column:type_size'] = "Cuota";

$bhlang['module:types'] = "Tipos de usuario";
$bhlang['moduledesc:types'] = "Edita tipos de usuario";

$bhlang['column:edit_quota'] = "Editar cuota (en MB)";
$bhlang['column:delete'] = "Borrar";

$bhlang['notice:type_updated'] = "Tipo creado o actualizado.";

