<?php

/*
 * ByteHoard 2.1
 * Copyright (c) Andrew Godwin & contributors 2004
 *
 *   Language file
 *   $Id: en.lang.php,v 1.11 2005/07/28 22:38:50 andrewgodwin Exp $
 *
 */
 
#name English
#author Andrew Godwin
#description The (default) english language file.

$bhlang['module:main'] = "P�gina Principal";
$bhlang['module:login'] = "Entrar";
$bhlang['module:logout'] = "Sair";
$bhlang['module:find'] = "Encontrar Arquivo";

$bhlang['title:login'] = "Entrar";
$bhlang['explain:login'] = "Favor fornecer seu nome de usu�rio e senha para entrar no sistema.";
$bhlang['label:username'] = "Nome do Usu�rio: ";
$bhlang['label:password'] = "Senha: ";
$bhlang['button:login'] = "Entrar";

$bhlang['title:folders'] = "pastas";

$bhlang['title:viewing_directory'] = "Visualizar diret�rio:";
$bhlang['error:not_a_dir'] = "O caminho forneciso n�o � um diret�rio.";

$bhlang['error:nothing_in_toolbar'] = "Sua barra de ferramentas n�o cont�m �tens. Isto � um erro fatal; comunique-se com seu administrador.";

$bhlang['error:not_a_file'] = "O arquivofornecido n�o � do tipocerto para esta a��o.";
$bhlang['error:no_file_specified'] = "Esta a��o precisa de um arquivi, mas parece que voce veio aqui sem um arquivo.";
$bhlang['title:viewing_file'] = "Arquivo: ";

$bhlang['module:download'] = "Download";
$bhlang['moduledesc:download'] = "Download este arquivo";

$bhlang['module:delete'] = "Delete";
$bhlang['moduledesc:delete'] = "Remover este arquivo";

$bhlang['label:_files'] = " Arquivos";

$bhlang['title:error'] = "Erro";
$bhlang['explain:error_occured'] = "Um erro ocorreu. Isto foi registrado, mas notifique o administrador se voce pensa que isto � s�rio.";

$bhlang['error:page_not_exist'] = "A p�gina n�o existe!";
$bhlang['error:file_not_exist'] = "Este arquivo n�o existe!";
$bhlang['error:directory_no_exist'] = "Este diret�rio n�o existe!";

$bhlang['title:deleting_'] = "Deletando ";
$bhlang['explain:delete'] = "Voce tem certeza que deseja deletar este arquivo?";

$bhlang['button:delete_file'] = "Deletar Arquivo";
$bhlang['button:cancel'] = "Cancelar";
$bhlang['notice:file_deleted'] = "Arquivo Deletado.";

$bhlang['error:access_denied'] = "Voce n�o tem permiss�o para fazer isto.";

$bhlang['title:upload'] = "Enviar Arquivos";

$bhlang['explain:upload'] = "Favor selecionar o arquivo que vove deseja enviar, ent�o clik no bot�o enviar para come�ar o processo. <br><br>Favor notar que enquanto o arquivo estiver sendo enviado nada acontecer� nesta tela; entretanto, a barra de marca��o do seu browser dever� mostrar o progresso do envio.";
$bhlang['module:upload'] = "Enviar Arquivos";
$bhlang['moduledesc:upload'] = "Transferir arquivos do seu computador";

$bhlang['button:upload'] = "Enviar";
$bhlang['label:uploading_to'] = "Enviar para: ";
$bhlang['button:change_folder'] = "Trocar";
$bhlang['title:choose_folder'] = "Trocar diret�rio";

$bhlang['error:no_write_permission'] = "Voce n�o tem permiss�o para escrever neste arquivo.";

$bhlang['module:url'] = "URL do Arquivo";
$bhlang['moduledesc:url'] = "Mostrar a URL dos arquivos";

$bhlang['explain:the_url_to_that_file_is_'] = "A URL para este arquivo �: ";
$bhlang['notice:file_#FILE#_upload_success'] = "Arquivo #FILE# enviado com sucesso.";

$bhlang['notice:logged_out'] = "Voce agora est� desconectado.";

$bhlang['log:#USER#_logged_out'] = "Sair para #USER#";
$bhlang['notice:logged_in_as_#USER#'] = "Conectado como #USER#. Seja Bem Vindo.";
$bhlang['notice:login_failed'] = "O Nome de usu�rio e/ou senha est�o(�) incorretos(o).";
$bhlang['log:failed_login_#USER#'] = "Erro ao se conectar como #USER#";
$bhlang['log:successful_login_#USER#'] = "Sucesso ao se conectar como #USER#";
$bhlang['log:#USER#_uploaded_#FILE#'] = "Arquivo enviado #FILE# para #USER#";

$bhlang['title:add_folder'] = "Adicionar diret�rio";
$bhlang['module:addfolder'] = "Adicionar diret�rio";
$bhlang['moduledesc:addfolder'] = "Vamos criar um diret�rio";
$bhlang['explain:add_folder'] = "Voce est� criando um novo diret�rio. Selecione um diret�rio no qual voce gostaria de cria-lo, e ent�o escolha o nome do seu novo diret�rio.";
$bhlang['button:add_folder'] = "Criar Novo Diret�rio";
$bhlang['label:folder_name'] = "Nome do Diret�rio: ";
$bhlang['label:create_in'] = "Criar em: ";
$bhlang['log:#USER#_denied_#PAGE#'] = "Acesso negada a #PAGE# para #USER#";

$bhlang['log:#USER#_created_#FOLDER#'] = "Diret�rio #FOLDER# criado por #USER#";
$bhlang['notice:folder_created'] = "Diret�rio Criado.";

$bhlang['notice:file_#FILE#_upload_failure'] = "O envio de arquivo #FILE# falhou.";
$bhlang['log:#USER#_failed_upload_#FILE#'] = "O envio do arquivo #FILE# falhou para #USER#";

$bhlang['explain:edit'] = "Fa�a as suas modifica��es no arquivo abaixo e prensione salvar para salvar as modifica��es. Observe que se isto n�o for um arquivo de texto (por exemplo, se ele n�o for um documento de processador de textos ou uma planilha), � prov�vel que este editor seja in�til, e tentando salvar pode corromper o arquivo.";
$bhlang['title:editing_#FILE#'] = "Editando #FILE#";
$bhlang['button:save'] = "Salvar";

$bhlang['module:edit'] = "Editar";
$bhlang['moduledesc:edit'] = "Editar o arquivo (em modo texto)";

$bhlang['notice:file_saved'] = "Arquivo Salvo.";
$bhlang['log:#USER#_modified_#FILE#'] = "Arquivo #FILE# modificado por #USER#";

$bhlang['module:htmledit'] = "Editor HTML";
$bhlang['moduledesc:htmledit'] = "Editar o arquivo (em modo HTML)";

$bhlang['explain:htmledit'] = "Fa�a suas mudan�as na p�gina abaixo e presione salvar para armzenalas.";

$bhlang['label:copy_to'] = "Copiar para: ";
$bhlang['explain:copy'] = "Selecione para onde voc� gostaria de copiar o arquivo, e o seu novo nome, ent�o pressione copiar.";
$bhlang['label:new_name'] = "Novo Nome: ";
$bhlang['button:copy'] = "Copiar";
$bhlang['title:copying_#FILE#'] = "Copioando #FILE#";
$bhlang['log:#USER#_copied_#FILE#_to_#DEST#'] = "Arquivo #FILE# copiado para #DEST# por #USER#";
$bhlang['notice:file_copied'] = "Arquivo copiado.";
$bhlang['module:copy'] = "Copiar";
$bhlang['moduledesc:copy'] = "Copiar o arquivo para um novo diret�rio";

$bhlang['title:uploading'] = "Enviando";
$bhlang['explain:uploading'] = "Oenvioest� em progresso. Isto pode demorar um pouco.<br>Esta janela ir� fechar quando o arquivo for copiado.";

$bhlang['notice:you_must_be_admin'] = "Voce tem que ser admninistrador para acessar esta �rea.";
$bhlang['title:__administration'] = " [Administra��o]";

$bhlang['title:welcome_to_administration'] = "Bem Vindo a Administra��o do ByteHoard";
$bhlang['explain:welcome_to_administration'] = "Bem Vindo a �rea de administra��o do ByteHoard. Selecione uma se��o para usar uma das conex�es acima ou abaixo.";

$bhlang['module:users'] = "Usu�rios";
$bhlang['moduledesc:users'] = "Administrar Usu�rios e Grupos";

$bhlang['title:user_administration'] = "Administra��o de Usu�rios";
$bhlang['explain:user_administration'] = "Selecione um grupo ou Tudo abaixo para examinar os usu�rios naquele grupo / todos os usu�rios. Para editar um usu�rio, clique no seu nome de usu�rio.";

$bhlang['label:disk_space_used'] = "Espa�ode Disco Usado";
$bhlang['label:upload_bandwidth_used'] = "Largura de banda usada na transfer�ncia de dados";
$bhlang['label:download_bandwidth_used'] = "Largura de banda usada no Download";

$bhlang['title:views'] = "visualizadores";

$bhlang['module:sharing'] = "Compartilhando";
$bhlang['moduledesc:sharing'] = "Compartilhar este com outros usu�rios";

$bhlang['title:sharing_'] = "Compartilhamento de ";
$bhlang['explain:sharing'] = "Aqui voc� pode configurar quem mais pode ver este arquivo, bem como o que eles podem fazer.";
$bhlang['label:users'] = "Usu�rios";
$bhlang['label:groups'] = "Grupos";
$bhlang['label:public'] = "Todos";
$bhlang['explain:no_users_sharing_to'] = "Voce n�o est� compartilhando com nenhum usu�rio.";
$bhlang['explain:no_groups_sharing_to'] = "Voce n�o est� compartilhando com nenhum grupo.";

$bhlang['label:sharing_hidden'] = "Esconder";
$bhlang['explain:sharing_hidden'] = "Este arquivo est� escondido.";

$bhlang['label:sharing_viewable'] = "Visualiz�vel";
$bhlang['explain:sharing_viewable'] = "Este arquivo pode ser visto ecopiado mas n�o pode ser modificado.";

$bhlang['label:sharing_writable'] = "Modific�vel";
$bhlang['explain:sharing_writable'] = "Este arquivo pode ser visto, movido, modificado ou deletado.";

$bhlang['label:sharingfolder_hidden'] = "Escondido";
$bhlang['explain:sharingfolder_hidden'] = "Este diret�rio est� escondido.";

$bhlang['label:sharingfolder_viewable'] = "Visualiz�vel";
$bhlang['explain:sharingfolder_viewable'] = "Este diret�rio pode ser visto e copiado mas n�o pode ser modificado.";

$bhlang['label:sharingfolder_writable'] = "Escrev�vel";
$bhlang['explain:sharingfolder_writable'] = "Este diret�rio pode ser visto, movido, modificado ou deletado.";

$bhlang['label:change_to'] = "Midificar para: ";

$bhlang['notice:permissions_changed'] = "Permiss�es Trocadas";

$bhlang['label:add_user'] = "Adicionar Usu�rio:";
$bhlang['label:add_group'] = "Adicionar Grupo:";
$bhlang['button:add'] = "Adicionar";

$bhlang['button:edit'] = "Editar";
$bhlang['button:delete'] = "Deletar";

$bhlang['column:user_type'] = "Topo";
$bhlang['column:used_space'] = "Espa�o do Usu�rio";
$bhlang['column:bandwidth_30_days'] = "Largura de Banda (�ltimos 30 dias)";
$bhlang['column:actions'] = "A��es";
$bhlang['column:username'] = "Nome do Usu�rio";

$bhlang['explain:delete_user'] = "S�o voc� seguro voc� quer eliminar irreversivelmente este usu�rio (e os seus arquivos)?";
$bhlang['button:delete_user'] = "Deletar Usu�rio";
$bhlang['button:delete_user_and_files'] = "Deletar usu�rioe seus arquivos";

$bhlang['notice:user_deleted'] = "Usu�rio deletado.";
$bhlang['notice:user_and_files_deleted'] = "Usu�rio e seus arquivos deletados.";

$bhlang['title:settings'] = "Configura��es";
$bhlang['explain:settings'] = "Aqui voc� pode modificar as configura��es b�sicas do ByteHoard. Selecione/edite as op��es que voc� quer e logo clik 'Salvar Configura��es' para us�-las.";

$bhlang['label:settings_usetrash'] = "Usar Lixo?";
$bhlang['explain:settings_usetrash'] = "Se Sim, os arquivos n�o s�o permantemente eliminados mas movidos a um diret�rio de Lixo.";

$bhlang['label:yes'] = "Sim";
$bhlang['label:no'] = "N�o";

$bhlang['button:save_settings'] = "Salvar Configura��es";

$bhlang['module:settings'] = "Configura��es";
$bhlang['moduledesc:settings'] = "Modifica��es de  Configura��es de Base";

$bhlang['label:settings_sitename'] = "Nome do Site";
$bhlang['explain:settings_sitename'] = "O nome do site (exposto no site da web e em mensagens)";

$bhlang['label:settings_limitthumbs'] = "Somente thumbnail de pequenos arquivos?";
$bhlang['explain:settings_limitthumbs'] = "Se configurar para N�o, as grandes imagens podem causar que o sistema reduza a velocidade ou pare enquanto prepara o thumbnail.";

$bhlang['notice:settings_saved'] = "Configura��es Salvas.";

$bhlang['title:delete_user'] = "Deletar Usu�rio";
$bhlang['title:edit_user'] = "Editar Usu�rio";

$bhlang['title:settings'] = "Configura��es";

$bhlang['explain:edit_user'] = "Fa�a as suas modifica��es a este usu�rio, logo clique 'Salvar Usu�rio' para salvar as modifica��es. <br>Se voc� n�o quer modificar a senha, somente deixar o espa�o das caixas em branco.";

$bhlang['subtitle:details'] = "Detalhes";

$bhlang['title:editing_user_'] = "Editando Usu�rio: ";
$bhlang['label:email'] = "Endere�o de Email:";
$bhlang['label:full_name'] = "Nome Completo:";
$bhlang['label:user_type'] = "Tipode Usu�rio:";

$bhlang['value:guest'] = "Convidado";
$bhlang['value:normal'] = "Normal";
$bhlang['value:admin'] = "Administrator";

$bhlang['subtitle:password'] = "Senha";
$bhlang['label:new_password'] = "Nova Senha:";
$bhlang['label:repeat_new_password'] = "Repetir Nova Senha:";

$bhlang['button:save_user'] = "Salvar Usu�rio";

$bhlang['error:passwords_dont_match'] = "As senhas n�o coincidem!";
$bhlang['notice:user_updated'] = "Usu�rio Atualizado!";

$bhlang['title:signup'] = "Registrar";
$bhlang['explain:signup'] = "Preencha os campos abaixo e pressione 'Registrar' para registro nosistema.";

$bhlang['label:repeat_password'] = "Repetie Senha:";
$bhlang['button:signup'] = "Registrar!";

$bhlang['module:signup'] = "Registrar";
$bhlang['moduledesc:signup'] = "Registrar uma conta";

$bhlang['error:username_in_use'] = "Este Nome de Usu�rio j� foi escolhido. Tente o outro.";
$bhlang['notice:signup_successful_can_login'] = "Sucesso! Voc� pode agora entrar usando a senha e o Nome de Usu�rio que voc� forneceu.";
$bhlang['log:user_signed_up_'] = "O usu�rio com sucesso registrou-se com seu Nome de Usu�rio ";
$bhlang['error:password_empty'] = "Voc� n�o introduziu uma senha!";

$bhlang['notice:file_description_saved'] = "Descri��o de arquivo salva!";
$bhlang['title:editing_description_#FILE#'] = "Edi��o de descri��o de #FILE#";
$bhlang['explain:editdesc'] = "Edite a descri��o deste arquivo abaixo (isto aparecer� em listagens de diret�rios). Deixe o espa�o em branco para usar uma descri��o autom�tica.";

$bhlang['button:savedesc'] = "Savar Descri��o";
$bhlang['module:editdesc'] = "Editar Descri��o";
$bhlang['moduledesc:editdesc'] = "Modificar a descri��o deste arquivo";

$bhlang['title:appearance']  = "Aparencia";
$bhlang['explain:appearance'] = "Aqui voc� pode modificar a apar�ncia de ByteHoard selecionando entre os formatos que voc� instalou. Selecione aquele que voc� deseja usar clicando no bot�o 'Usar Este Formato'; o formato que voc� esta utilizando correntemente ser� nomeado de 'FormatoCorrente', e n�o tem esta op��o.<br><br>Tamb�m se lembre de que o centro de administra��o sempre parecer� o mesmo; voc� ter� de ver o sistema principal para ver as modifica��es.";

$bhlang['label:author'] = "Autor: ";

$bhlang['module:appearance'] = "Apar�ncia";
$bhlang['moduledesc:appearance'] = "Editar a apar�ncia do sistema";

$bhlang['explain:current_skin'] = "Apar�ncia Corrente";

$bhlang['button:use_this_skin'] = "Usar Esta Apar�ncia";
$bhlang['notice:skin_changed'] = "Apar�ncia Modificada.";

$bhlang['label:settings_signupmoderation'] = "Moderar registros de usu�rio?";
$bhlang['explain:settings_signupmoderation'] = "Se estabelecer sim um administrador deve aprovar todos os novos usu�rios.";

$bhlang['error:validation_link_wrong'] = "Desculpe, a conex�o de valida��o que voc� clicou n�o � v�lida. Ela pode ter vencido; neste caso, tente conectar-se novamente.";
$bhlang['log:user_validated_'] = "O usu�rio com sucesso validou o nome de usu�rio ";
$bhlang['log:user_signup_m_pending_'] = "Modera��o agora pendente para nome de usu�rio ";

$bhlang['notice:moderation_now_pending'] = "O seu pedido de registro foi com sucesso validado e submetido ao administrador (es) da aprova��o. Voc� ser� notificado quando o seu pedido for aceito ou rejeitado.";
$bhlang['notice:do_email_validation'] = "Uma mensagem foi enviada ao endere�o que voc� forneceu com os detalhes de como voc� deve fazer para continuar com oseu registro.";

$bhlang['emailsubject:registration_validation'] = "#SITENAME# valida��o de registro";


# Note to translators: Just the stuff inside the double quotes, it's allowed to go over multiple lines.
# Another note: Lines beginning with a hash (#) are ignored. So this line is ignored. And the one above.
$bhlang['email:registration_validation'] = "O seu pedido em uma conta est� esperando por valisa��o.

Para verificar a sua conta, visite o endere�o seguinte no seu browser:

#LINK#

Se voc� n�o validar esta conta dentro de sete dias o seu registro ser� eliminado.";




$bhlang['error:email_error'] = "Estamos com problemas com o sistema de correio eletr�nico; o seu pedido n�o est� podendo ser conclu�do agora. Por favor tente novamente depois.";

$bhlang['notice:validation_already_done_pending_approval'] = "Voc� j� validou a sua conta! Ele est� esperando atualmente a aprova��o.";

$bhlang['title:registrations_administration'] = "Administra��o de Registros";
$bhlang['module:registrations'] = "Registros";
$bhlang['moduledesc:registrations'] = "Aprove ou negue novos registros de usu�rio";
$bhlang['explain:registration_administration'] = "Isto � uma lista de todos os registros verificados, pendentes. Os detalhes de cada usu�rio em perspectiva s�o mostrados, junto com op��es para aceitar ou rejeitar a sua aplica��o. <br>Tenha cuidado, como n�o h� nenhuma confirma��o de nenhuma a��o.";

$bhlang['notice:registration_moderation_off'] = "A modera��o de registro � desligada; todos os usu�rios s�o sendo auomaticamente aprovados depois que eles s�o verificados. Para usar essa op��o, v� a Cofigura��es e Modifica��o 'Moderam registros de usu�rio?' e seleciona 'Sim'.";

$bhlang['button:reject'] = "Rejeitar";
$bhlang['button:accept'] = "Aceitar";

$bhlang['notice:#USER#_accepted'] = "#USER# aceito.";
$bhlang['notice:#USER#_rejected'] = "#USER# rejeitado.";

$bhlang['emailsubject:registration_accepted'] = "Registro Aceito para #SITENAME#";
$bhlang['email:registration_accepted'] = "O seu registro foi aceito por um administrador.

Voc� pode entrar com os detalhes seguintes:
Nome do Usu�rio: #USERNAME#
Senha: Comovoce entro no formul�rio de registro.

N�o podemos recuperar a sua senha para voc�, pois ela  � encriptada no nosso banco de dados, mas � poss�vel reinicializ�-lo; veja o site para mais detalhes.

Esperamos que voc� goste do servi�o.";

$bhlang['emailsubject:registration_rejected'] = "Registro Rejeitado para #SITENAME#";
$bhlang['email:registration_rejected'] = "O seu registro n�o foi aprovado; a sua conta de usu�rio n�o ser� criada.

Voc� � livre para reaplicar; contudo, se a sua aplica��o n�o foi aceita a primeira � improv�vel que ele seja aceita na segunda vez.";


$bhlang['error:registration_doesnt_exist'] = "Esse registro n�o mas existe. Pode ter sido aceito ou rejeitado por algu�m mais.";

$bhlang['error:username_too_long'] = "Nome de Usu�rio demasiado longo. Ele deve ter menos de 255 carateres de comprimento.";

$bhlang['button:go'] = "Ir";
$bhlang['button:request_reset'] = "Recompor Pedido";

$bhlang['title:recover_password'] = "Recuperar Senha";
$bhlang['explain:recover_password'] = "Use este formulario para recuperar o seu nome de usu�rio ou a senha. Introduza o seu nomede usu�rio ou mande por correio eletr�nico o seu endere�o de e-mail na caixa abaixo e clik 'Ir' para solicitar uma recupera��o. Voc� receber� um -mail que cont�m o seu nomede usu�rio e uma conex�o para reinicializar a sua senha.";

$bhlang['text:or'] = "Ou";

$bhlang['module:passreset'] = "Detalhes Perdidos";
$bhlang['moduledesc:passreset'] = "Recupere o seu nome de usu�rio ou reinicialize a sua senha";

$bhlang['emailsubject:passreset_request'] = "Pedido de Recomposi��o de Senha em #SITENAME#";
$bhlang['email:passreset_request'] = "Voc�, ou algu�m que pretende ser voc�, solicitou uma recomposi��o de senha no site. Para reinicializar a sua senha, visite este endere�o no seu browser:

#LINK#

Se voc� n�o pediu esta recomposi��o, ignore este e-mail. O pedido ser� eliminado em 48 horas.";

$bhlang['error:username_doesnt_exist'] = "Iste nome de usu�rio n�o existe no nosso banco de dados!";
$bhlang['error:email_doesnt_exist'] = "Este e-mail n�o existe no nosso banco de dados!";

$bhlang['emailsubject:passreset_u_request'] = "Nome de Usu�rio e Senha pedido em  #SITENAME#";
$bhlang['email:passreset_u_request'] = "Voc�, ou algu�m que pretende ser voc�, solicitaram uma lembran�a dos seus detalhes no site.

Seu Nome de Usu�rio �: #USERNAME#

Para reinicializar a sua senha, visite este endere�o no seu browser:

#LINK#

Se voc� n�o pediu esta recomposi��o, ignore este e-mail. O pedido ser� eliminado em 48 horas.";

$bhlang['notice:passreset_request_sent'] = "O e-mail que cont�m novas instru��es foi enviado ao seu endere�o de correio eletr�nico.";

$bhlang['error:passreset_link_invalid'] = "Esta conex�o � inv�lida - ela expirou ou voc� copiou com erro.";

$bhlang['emailsubject:passreset_new_password'] = "Sua nova senha para #SITENAME#";
$bhlang['email:passreset_new_password'] = "Sua nova senha para este site �:

#PASSWORD#

Esta senha foi gerada aleatoriamente, e pode ser modificada quando voc� conectar.";

$bhlang['notice:passreset_new_password_sent'] = "A sua nova senha foi enviada ao seu endere�o de e-mail.";

$bhlang['error:username_invalid'] = "Este usu�rio� inv�lido.";

$bhlang['error:systemwrong'] = "Erro Fatal: A sua configura��o de sistema impede esta fun��o de funcionar.";

$bhlang['title:options'] = "Op��es";
$bhlang['module:options'] = "Op��es";
$bhlang['moduledesc:options'] = "Modificar suas Op��es";
$bhlang['explain:options'] = "Aqui voc� pode modificar v�rias op��es de apar�ncia dosistema e de como ele trabalha, bem como a modifica��o da sua senha e detalhes de perfil.";
$bhlang['title:change_password'] = "Modificar Senha";
$bhlang['title:interface_options'] = "Op��es de Interface";
$bhlang['title:profile'] = "Perfil";

$bhlang['label:old_password'] = "Senha Antiga: ";
$bhlang['button:change_password'] = "Trocar Senha";
$bhlang['error:old_password_invalid'] = "A sua antiga senha estava errada! Tente novamente.";
$bhlang['notice:password_changed'] = "Senha modificada com sucesso!";

$bhlang['error:unknown'] = "Um erro desconhecido ocorreu. Isto pode ser uma boa oportunidade de inform�-lo.";
$bhlang['warning:blank_password'] = "Voc� est� usando uma senha em branca! Isto � uma m� id�ia.";

# These are all options! We're not going to collect all this inforation about every user.
# The administrator will be able to choose which ones to allow.
$bhlang['profile:email'] = "Endere�o de Email";
$bhlang['profile:fullname'] = "Nome Completo";
$bhlang['profile:website'] = "Website";
$bhlang['profile:telephone'] = "N�mero do Telefone";
$bhlang['profile:fax'] = "N�mero do Fax";
$bhlang['profile:jabber'] = "Jabber ID";
$bhlang['profile:icq'] = "ICQ#";
$bhlang['profile:msn'] = "MSN ID";
$bhlang['profile:aim'] = "AIM ID";
$bhlang['profile:yahoo'] = "Yahoo! ID";
$bhlang['profile:nickname'] = "Apelido";
$bhlang['profile:position'] = "Posi��o";
$bhlang['profile:location'] = "Localiza��o";
$bhlang['profile:gender'] = "Sexo";
$bhlang['profile:address'] = "Endere�o";
$bhlang['profile:postcode'] = "C�digo Postal";
$bhlang['profile:zipcode'] = "C�digo Postal";
$bhlang['profile:nationality'] = "Nationalidade";
$bhlang['profile:latitude'] = "Latitude";
$bhlang['profile:longitude'] = "Longitude";
$bhlang['profile:occupation'] = "Ocpa��o";
$bhlang['profile:status'] = "Estado";

$bhlang['button:save_profile'] = "Salvar Perfil";
$bhlang['notice:profile_saved'] = "Perfil salvo.";

$bhlang['error:cannot_determine_update_server'] = "O servidor de atualiza��o n�o pode ser localizado. � prov�vel que o site ByteHoard esteja com problemas; por favor tente novamente depois.";
$bhlang['error:cannot_download_package'] = "N�o podem ser baixados os arquivo de pacote do servidor remoto.";
$bhlang['error:system_setup_wrong'] = "O seu sistema � incapaz de os arquivos solicitados. Voc� deve fazer isto manualmente.";
$bhlang['error:cannot_write_to_package'] = "N�o pode escrever ao arquivo de pacote. Verifique que as permiss�es dos diret�rios no ByteHoard est�o corretamente estabelecidas.";

$bhlang['title:folder_files'] = "Arquivos";
$bhlang['title:folder_actions'] = "A��es no Diret�rio";
$bhlang['module:deletefolder'] = "Deletar";
$bhlang['moduledesc:deletefolder'] = "Deletar este diret�rios e seus arquivos";
$bhlang['module:copyfolder'] = "Copiar";
$bhlang['moduledesc:copyfolder'] = "Copiar este diret�riopara outro local";
$bhlang['module:sharingfolder'] = "Compartilhando";
$bhlang['moduledesc:sharingfolder'] = "Compartilhar este diret�tio com outros usu�rios";
$bhlang['notice:folder_deleted'] = "Diret�rioDeletado";

$bhlang['error:cannot_delete_that'] = "Voce n�o pode deletar este diretorio ou arquivo.";

$bhlang['button:delete_folder'] = "Deletar Diret�rio";
$bhlang['explain:deletefolder'] = "Voce realmente quer eliminar esta pasta e todos os seus conte�dos?";

$bhlang['label:settings_fromemail'] = "Sistema de : Endere�o: ";
$bhlang['explain:settings_fromemail'] = "O endere�o do qual todos os correios eletr�nicos de sistema parecem vir.";
$bhlang['label:settings_imageprog'] = "Programa de imagem para usar: ";
$bhlang['explain:settings_imageprog'] = "Tem que ser o 'imagemagick' ou 'gd'.";
$bhlang['label:settings_syspath_convert'] = "Caminho para 'converter': ";

# Translators; Leave $"."PATH as it is.
$bhlang['explain:settings_syspath_convert'] = "Somente requerido se voce selecionar 'imagemagick' como programa de imagem.<br>Voce pode usar 'converter' se o programa estiver no caminho $"."PATH.";

$bhlang['label:settings_fileroot'] = "Diret�rio Virtual do Sistema de Arquivos: ";
$bhlang['explain:settings_fileroot'] = "O diret�rio no qual os arquivos ByteHoard s�o armazenados.<br><b>N�o MODIFICAR</b> a menos que voc� saiba o que voc� est� fazendo.<br>O caminho n�o DEVE terminar em uma barra.";

$bhlang['error:no_gd'] = "O GD n�o est� instalado ainda, GD � selecionado para thumbnailing. Isto � fatal.";

$bhlang['explain:sharingfolder'] = "Aqui voc� pode configurar quem mais pode ver este diret�rio, bem como o que eles podem fazer. <br>Observe que todas as modifica��es feitas a esse diret�rio afetar�o todos os arquivos que est�o nele.<br>Por essa raz�o, n�o recomendamos compartilhar o seu diret�rio principal.";

$bhlang['label:sharingfolder_owner'] = "Dono";
$bhlang['explain:sharingfolder_owner'] = "Tem o m�ximo controle sobre o diret�rio.";
$bhlang['label:sharing_owner'] = "Dono";
$bhlang['explain:sharing_owner'] = "Tem o m�ximo controle sobre o arquivo.";

$bhlang['error:permissions_self'] = "Voce n�o pode modificar suas proprias permiss�es.";
$bhlang['notice:permissions_changed'] = "Permiss�es Modificadas.";
$bhlang['notice:permissions_group_added'] = "Grupo Adicionado.";
$bhlang['notice:permissions_user_added'] = "Usu�rio Adicionado.";
$bhlang['notice:permissions_group_deleted'] = "Gruop Deletado.";
$bhlang['notice:permissions_user_deleted'] = "Usu�rio Deletado.";

$bhlang['label:delete_user'] = "Deletar Usu�rio: ";
$bhlang['label:delete_group'] = "Deletar Grupo: ";

$bhlang['button:return'] = "Retornar";

$bhlang['module:returntofolder'] = "Retornar para Diret�rio";
$bhlang['moduledesc:returntofolder'] = "Visualizar diret�rio onde este arquivo est�";

$bhlang['notice:folder_copied'] = "Diret�rio Copiado.";

$bhlang['module:filelink'] = "Arquivo de E-mail";
$bhlang['moduledesc:filelink'] = "Enviar Email ou criar um link tempor�rio para este arquivo";
$bhlang['error:no_filepath'] = "Erro Fatal: Nenhum caminho fornecido.";

$bhlang['title:filemail'] = "Arquivo de E-mail";
$bhlang['explain:filemail'] = "Arquivo de E-mail permite que voc� mande por correio eletr�nico uma conex�o tempor�ria a este arquivo que vencer� depois de certo per�odo de tempo. <br><br>Preencha os campos em baixo com o recipiente do correio eletr�nico, o assunto e opcionalmente uma mensagem. A conex�o ser� automaticamente acrescentada para o fundo do e-mail. Para m�ltiplos recipientes voc� deve separar endere�os de correio eletr�nico com v�rgulas.";

$bhlang['label:subject'] = "Assunto: ";
$bhlang['label:message'] = "Mensagem: ";

$bhlang['error:no_emailaddr'] = "Voc� n�o forneceu um endere�o de correio eletr�nico.";
$bhlang['error:no_emailsubj'] = "Voc� n�o forneceu um assunto do e-mail.";
$bhlang['error:invalid_email_#EMAIL#'] = "O endere�o '#EMAIL#' � inv�lido.";

$bhlang['notice:email_sent'] = "Email enviado com sucesso.";
$bhlang['notice:email_sent_to_#EMAIL#'] = "Email enviado com sucesso para #EMAIL#.";

$bhlang['text:days'] = " dias";
$bhlang['label:expires_in'] = "Expira em: ";

$bhlang['text:max_#NUM#_days'] = "(maximo #NUM# dias)";

$bhlang['error:expires_invalid'] = "Este tempo de vencimento � inv�lido. Ele deve ser um n�mero positivo.";
$bhlang['error:expires_too_much'] = "Este tempo de vencimento est� acima do limite m�ximo.";

$bhlang['label:settings_maxexpires'] = "Tempo de vencimento de Arquivo de E-mail  M�ximo (dias): ";
$bhlang['explain:settings_maxexpires'] = "O n�mero m�ximo de dias da conex�es ao Arquivo e E-mail pode ser estabelecido para ser v�lido para.";

$bhlang['error:no_filecode'] = "Voc� n�o forneceu uma conex�o com um c�digo de arquivo.";
$bhlang['error:filecode_invalid'] = "A conex�o que voc� forneceu � inv�lida ou venceu.";

$bhlang['title:filelinks'] = "Administra��o de Link de Arquivo";
$bhlang['module:filelinks'] = "Arquivo de E-mail";
$bhlang['moduledesc:filelinks'] = "Administra��o de Link de Arquivo";
$bhlang['explain:filelinks'] = "Esta p�gina fornece um resumo das conex�es de Arquivos de E-mail ativas. <br>Eles s�o ordenadas por nome de usu�rio, e embaixo de cada nome de usu�rio est� uma lista das suas conex�es, junto com o endere�o de correio eletr�nico conectado ao arquivo, e o tempo at� que a conex�o ir� vencer. <br>Voc� pode examinar a conex�o clicando a conex�o 'Link' ao lado da entrada, e voc� pode desativar uma conex�o clicando a conex�o 'Deletar'.";

$bhlang['column:expires_in'] = "Expira em";
$bhlang['column:email'] = "Endere�o de Email";
$bhlang['column:file'] = "Arquivo";

$bhlang['button:link'] = "Link";

$bhlang['notice:filelink_deleted'] = "Link de Arquivo de E-mail deletado.";

$bhlang['log:filelink_denied'] = "Negar acesso para #FILELINK#";
$bhlang['log:filelink_accessed'] = "#FILEPATH# acessado pelo Link de Arquivo[#FILELINK#].";

$bhlang['label:settings_authmodule'] = "M�dulo de Autentica��o: ";
$bhlang['explain:settings_authmodule'] = "O m�dulo usado para autenticar usu�rios. Deixe como padr�o 'bytehoard.inc.php' se voc� n�o tem de autenticar contra outra coisa alem do que ByteHoard.";

$bhlang['button:send'] = "Enviar";
$bhlang['button:back'] = "Voltar";


$bhlang['label:settings_baseuri'] = "ByteHoard Web URL (opcional): ";
$bhlang['explain:settings_baseuri'] = "O URL do diret�rio bytehoard principal (deve incluir uma barra). Isto � opcional; deixe-o espa�o em branco para usar um valor auto-detectado.";

$bhlang['title:overwriting_'] = "Sobrescrever ";
$bhlang['explain:overwrite'] = "O arquivo do que voc� acaba de transferir tem o mesmo nome que aquele que j� existe. O que gostaria de fazer?";

$bhlang['label:linkonly'] = "Crie a s� a conex�o (n�o o mande por correio eletr�nico): ";

$bhlang['text:link__expire_in_#EXPIRE#'] = "A conex�o (vencer� em #EXPIRE# dias): ";

$bhlang['label:html'] = "HTML: ";
$bhlang['label:bbcode'] = "BBcode: ";
$bhlang['title:image_tags'] = "Etiqueta de Imagens";

$bhlang['error:email_empty'] = "Voc� n�o introduziu um endere�o de e-mail.";

$bhlang['email:filemail_footer'] = "---------------------------------------------
O arquivo estar� dispon�vel para o download at� #DATE#

Anexo: #FILENAME#, #FILESIZE# (MD5: #MD5#)
#LINK#

Voc� recebeu uma conex�o de anexo de arquivo dentro deste e-mail enviado via #SYSTEMNAME#. 
Para recuperar o anexo, por favor clique na conex�o.";

$bhlang['label:downloadnotice'] = "Envie E-mails de Notifica��o de Download: ";

$bhlang['emailsubject:filemail_link_accessed'] = "Notifica��o de Acesso de Conex�o de Arquivo de E-mail (#FILENAME#)";

$bhlang['email:filemail_link_accessed'] = "Isto � uma notifica��o automatizada.

Uma das suas conex�es de Arquivo de E-mail foi acessada. Voc� solicitou a notifica��o de qualquer acesso a este arquivo quando voc� criou a conex�o.

O arquivo (#FILEPATH#) foi acessado �s #TIME#, do endere�o IP #IP#. O e-mail que voc� enviou esta conex�o foi #EMAIL#.

Nota: ESte link expira em #EXPIRES#.";




$bhlang['install:title:bytehoard_installation'] = "Instala��o do ByteHoard";

$bhlang['install:title:page_not_found'] = "P�gina N�o Encontrada";
$bhlang['install:error:page_not_found'] = "Esta p�gina n�o existe!";
$bhlang['install:title:menu'] = "Menu";
$bhlang['install:menu:install'] = "Instalar ByteHoard";
$bhlang['install:menu:upgrade'] = "Atualizar ByteHoard";
$bhlang['install:menu:documentation'] = "Documenta��o";
$bhlang['install:menu:systeminfo'] = "Informa��o do Sistema";

$bhlang['install:text:install_intro'] = "Bem Vindo ao ByteHoard ".$bhconfig['version'].".<br><br> As seguintes p�ginas o guiar�o pelo processo de instala��o. A organiza��o � relativamente f�cil; o sistema tentar� gui�-lo por ele tanto quanto poss�vel. Voc� ter� de ter os detalhes da sua senha de entrada de banco de dados, bem como assegurar que o seu sistema supra as exig�ncias minimas para instalar ByteHoard.<br><br>Prosseguindo com a instala��o, voc� aceita � licen�a deste software - a GNU GPL v2 (uma c�pia  voc� pode encontrar incluso com o arquivo LICEN�A abaixo, ou em <a href='http://www.gnu.org/copyleft/gpl.html'>http://www.gnu.org/copyleft/gpl.html</a>.<br><br>Para o suporte, atualiza��es, os extras e as not�cias no desenvolvimento verifique o nosso site web em <a href='http://bytehoard.org'>bytehoard.org</a>";

$bhlang['install:title:introduction'] = "Introdu��o";
$bhlang['install:title:systemchecks'] = "Verificar Sistema";
$bhlang['install:text:checking_system'] = "Verificando sistema...";
$bhlang['install:label:php'] = "PHP";
$bhlang['install:label:extensions'] = "Extens�es";
$bhlang['install:label:external_progs'] = "Programas Externos";
$bhlang['install:label:filesystem'] = "Arquivo de Sistema";
$bhlang['install:check:php'] = "Vers�o PHP: ";
$bhlang['install:check:safe_mode'] = "Modo seguro desligado: ";
$bhlang['install:check:pcre'] = "PCRE: ";
$bhlang['install:check:gd'] = "GD: ";
$bhlang['install:check:imagemagick'] = "ImageMagick: ";

$bhlang['install:check:php5'] = "PHP5 (n�o est� completamente testado)";
$bhlang['install:check:failed'] = "Falhou";
$bhlang['install:check:failedoption'] = "Falhou (opcional)";
$bhlang['install:check:ok'] = "OK";

$bhlang['install:check:failedphp'] = "Falhou (requer PHP 4.2 or maior)";
$bhlang['install:check:failedsafemode'] = "Falhou (modo seguro ligadi)";
$bhlang['install:check:failedperm'] = "Falhou (arquivo n�o aceita escrita, checar permiss�es)";

$bhlang['install:check:config.inc.php'] = "config.inc.php aceita escrita: ";
$bhlang['install:check:cache'] = "cache/ aceita escrita: ";

$bhlang['install:text:checksok'] = "Todos os verifica��es parecem ter passado. Bom.";
$bhlang['install:text:checkswarnings'] = "Voc� parece ter alguns avisos, mas nada s�rio. Voc� pode continuar, ou voc� pode tentar instalar os componentes ausentes. Se voc� precisar de ajuda com isto, por favor sinta-se a vontade para visitar o site ByteHoard (bytehoard.org) e fazer-nos perguntas.";
$bhlang['install:text:checksfatals'] = "Um ou v�rios testes essenciais falharam. Por favor classifique esses antes de que voc� continue. Se voc� precisar de ajuda com algum desses problemas, ent�o por favor visite o site bytehoard.org, ser� um prazer ajud�-lo.";
$bhlang['install:text:thumbnails_disabled'] = "Nota: O Thumbnails ser� desabilitado. Por favor instale GD ou ImageMagick para usar thumbnails.";
$bhlang['button:next'] = "Pr�ximo";

$bhlang['install:title:choose_database'] = "Escolher o Banco de Dados";
$bhlang['install:title:configure_database'] = "Configurar o Banco de Dados";

$bhlang['install:title:system_information'] = "Informa��es do Sistema";

$bhlang['install:configdb:intro'] = "Voc� deve fornecer agora os seus detalhes de conex�o de banco de dados. Se voc� sabe de que eles s�o, fale com o seu administrador de sistema, ou se isto falhar, ou voc� n�o tem um, tente pedir-nos a ajuda.";
$bhlang['install:configdb:nont_needed'] = "Este m�dulo de banco de dados n�o tem nenhuma configura��o. Voc� pode proceder com o seguinte passo.";
$bhlang['install:title:save_database_configuration'] = "Salvar Configura��o do Banco de Dados...";
$bhlang['install:writeconfig:saved'] = "Configura��o de banco de dados salva com sucesso.";
$bhlang['install:writeconfig:not_saved'] = "Houve um erro salvando a configura��o de banco de dados. Por favor verifique as permiss�es no diret�rio ByteHoard.";
$bhlang['install:title:init_database'] = "Inicializando Banco de Dados";

$bhlang['install:title:test_database_configuration'] = "Testanto Configura��o do Bancode Dados...";
$bhlang['install:testdb:ok'] = "Configura��o parece ser v�lida.";
$bhlang['install:testdb:failed'] = "N�o pode conectar-se ao banco de dados! O erro est� abaixo.";
$bhlang['error:error_creating_table'] = "Erro criando tabela: ";
$bhlang['install:initdb:database_initialised'] = "Banco de Dados Inicializado.";

$bhlang['install:title:set_paths'] = "Configurar Caminho";

$bhlang['install:paths:intro'] = "Voc� agora tem de estabelecer os caminhos para o ByteHoard. Voc� ter� de estabelecer tanto URL do sistema como o diret�rio onde os arquivos de usu�rios ser�o armazenados.<br><br>O instalador ter� tentado adivinhar o seu URL; n�o obstante, verifique-o, as vezes partes extras aparecem ou as barras est�o faltando. O URL dado deve ter um http:// ou https:// prefixo, e uma barra (/) no final.<br><br>O diret�rio 'filestorage' padr�o como subdiret�rio no diret�rio do ByteHoard chamado 'filestorage'; isto n�o � totalmente seguro, e se voc� puder deve criar um diret�rio externo ao diret�rio da web, dar-lhe as permiss�es apropriadas, e fornecer o caminho. <br>Por favor observe que um caminho relativo ser� interpretado do diret�rio ByteHoard.";

$bhlang['install:paths:system_url'] = "URL do Sistema: ";
$bhlang['install:paths:file_storage_directory'] = "Diret�riode Armazenagem de Arquivo: ";

$bhlang['value:enabled'] = "N�o";
$bhlang['value:disabled'] = "Sim (O usu�rio n�o pode conectar)";

$bhlang['label:disabled'] = "Usu�rio desabilitado?: ";

$bhlang['install:title:save_paths'] = "Salvando os Caminhos...";

$bhlang['install:savepaths:message'] = "Caminhos Salvos.";

$bhlang['install:savepaths:error_baseuri'] = "� uma inv�lida URL. Por favor assegure que o URL inclui http: // ou https: // e que ele termina em uma barra.";
$bhlang['install:savepaths:error_fileroot'] = "� um caminho de diret�rio inv�lido.";

$bhlang['install:title:create_administrator'] = "Criar Administrador";

$bhlang['notice:login_failed_disabled'] = "Conec��o Falhou: A sua conta foi desabilitada.";

$bhlang['log:failed_login_disabled_#USER#'] = "Oa #USER# foi negada a conec��o (desabilitado)";

$bhlang['title:add_user'] = "Adicionar Usu�rio";
$bhlang['explain:add_user'] = "Use esta formul�rio para adicionar um novo usu�rio. Voc� deve introduzir um nome de usu�rio, senha e o e-mail; os outros campos s�o opcionais.";
$bhlang['button:add_user'] = "Adicionar Usu�rio";
$bhlang['notice:user_added'] = "Usu�rio adicionado com sucesso.";
$bhlang['label:homedir'] = "Diret�rio Principal: ";
$bhlang['value:normal_homedir'] = "Subdiret�rio abaixo / (pad�o)";
$bhlang['value:root_homedir'] = "/ (nenhum diret�rio principasl)";
$bhlang['label:groups'] = "Grupos Iniciais (separados por v�rgula): ";
$bhlang['module:adduser'] = "Adicionar Usu�rio";
$bhlang['moduledesc:adduser'] = "Adicinar um novo usu�rio ao sistema.";

$bhlang['install:createadmin:explain'] = "Um usu�rio administrador foi criado. Por favor guarde esse detalhescom seguran�a; voc� pode modificar a senha ou acrescentar um novo usu�rio administrador uma vez que voc� conectou.";


$bhlang['install:finish:explain'] = "Por favor observe: Voc� DEVE eliminar completamente o diret�rio instalar/ antes que voc� comece a usar ByteHoard. Se algu�m que tem o acesso a este diret�rio pode reinicializar o sistema com uma nova administra��o nome de usu�rio e senha, que eles saber�o, bem como possivelmente ser capaz de acessar o banco de dados tanto de ByteHoard como de outro software nesta m�quina. A partida dele � um risco de seguran�a grande. Se voc� deseja re-instalar ou fazer um upgrade, voc� pode extrair simplesmente o diret�rio instalar/  do arquivo. </b> <br> <br> N�s recomende que voc� conecte-se � �rea de administra��o primeiro e modifique as colnfigura��es para ajustar o seu sistema. Os URLs da �rea principal e a �rea de administra��o s�o mostrados em baixo.";
$bhlang['install:finish:label:url'] = "URL do Sistema: ";
$bhlang['install:finish:label:adminurl'] = "URL de Administra��o: ";

$bhlang['error:signup_disabled'] = "Aconec��o foi desabilitada.";
$bhlang['label:settings_signupdisabled'] = "Dasabilitar Conec��o?: ";
$bhlang['explain:settings_signupdisabled'] = "Se isto for verificado a op��o 'Signup' ser� inacess�vel.";

$bhlang['install:title:complete'] = "Completa";

$bhlang['install:title:bytehoard_upgrade'] = "Atualizar ByteHoard";

$bhlang['install:update:intro'] = "Bem-vindos ao Atualisador ByteHoard. Este programa far� uma atualiza��o da sua velha instala��o ByteHoard a esta vers�o.<br><br>Observe que fazer uma atualiza��o de voc� deve ter movido ou ter eliminado os conte�dos do velho diret�rio ByteHoard parte do arquivo <i>config.inc.php</i> e <i>filestorage/</i> ou diret�rio <i>userfiles/</i>, e logo voc� deve ter extra�do os novos arquivos ByteHoard naquele mesmo diret�rio (voc� deveria mov�-los da pasta como a que a extra��o gera, provavelmente chamado algo como <i>bytehoard-".str_replace(" ","-",strtolower($bhconfig['version']))."/</i>.<br><br>Por favor fa�a um backup dos seus velhos arquivos ByteHoard e banco de dados antes de executar esta opera��o; voc� deve fazer isto antes de instalar qualquer novo software, mas neste caso as opera��es de atualiza��o podem n�o trabalhar com sucesso, devido � grande variedade de sistemas para os quais eles s�o projetados para fazer uma atualiza��o.";

$bhlang['install:title:choose_old_version'] = "Escolha Velha Vers�o";

$bhlang['install:upgrade:choose_text'] = "Por favor selecione a vers�o da que voc� est� fazendo uma atualiza��o. Observe que sele��o da vers�o incorreta ter� resultados interessantes ....";

$bhlang['button:upgrade'] = "Atualizar";

$bhlang['install:title:upgrading'] = "Atualizando...";

$bhlang['install:error:no_upgradefrom'] = "Voc� n�o selecionou uma vers�o para fazer uma atualiza��o!";
$bhlang['install:error:invalid_upgradefrom'] = "Voc�  forneceu uma vers�o incorreta para fazer uma atualiza��o. Tente novamente.";

$bhlang['install:upgrade:complete'] = "Atualiza��o completa. Voc� pode usar agora o sistema; veja abaixo as notas espec�ficas de atualiza��o.";

$bhlang['install:upgrade:notes'] = "Notas:";

$bhlang['install:upgrade:specific:2.0.x_to_2.1.g'] = "ByteHoard 2.1 � uma grande diferen�a do 2.0. H� muitas novas caracter�sticas e alguns desapareceram desde a vers�o 2.0. As coisas que voc� deve observar incluem: <ul><li>A �rea de administra��o � acessada agora indo ao ByteHoard URL seguido por /administrator/ (Observe que uma conex�o 'centro de administra��o' deve aparecer de qualquer maneira).</li>
<li>Todos os seus usu�rios foram transferidos, e os seus velhos administradores permanecem como administradores. Contudo, qualquer registro pendente foi retirado.</li>
<li>Todas as suas velhas conex�es de Arquivo de E-mail continuar�o trabalhando at� que elas ven�am.</li>
<li>Todos os seus velhos grupos e os seus usu�rios foram transferidos.</li>
<li>Todos os Arquivos de E-mails agora v�m dos endere�os de e-mail dos usu�rios por padr�o.</li>
<li>Se as fun��es de e-mail foram apagadas eles retormam.</li>
<li>As quotas de espa�o de usu�rio foram retiradas, desde que isto ainda n�o est� implementado.</li></ul><br><br> Esperamos que voc� goste do ByteHoard 2.1.";

$bhlang['label:remind_in'] = "Lembrete: ";
$bhlang['text:days_before_expiry'] = "days before expiry (0 = No reminder)";

$bhlang['title:group_administration'] = "Group Administration";

$bhlang['explain:group_administration'] = "Here you can manage groups of users. <br><br>All current groups are listed below, with the users they contain. <br>To remove a user from a group, click 'Remove' next to their username. <br>To add a user to a group, enter their name and the group name in the form at the top and press 'Add'. <br><br>Please note that groups are simply collections of users; to create a new group, simply add a user to it (i.e. to create the new group 'friends' simply add a user to 'friends').<br> Also note that the group name 'All' is reserved for internal use, and that all group names will be converted to lowercase.";

$bhlang['button:remove'] = "Remove";
$bhlang['label:group'] = "Group: ";
$bhlang['button:add_to_group'] = "Add To Group";

$bhlang['module:admin'] = "Administration";
$bhlang['moduledesc:admin'] = "Takes you to the administration area.";

$bhlang['module:return'] = "Retornar ao BH";
$bhlang['moduledesc:return'] = "Volte ao sistema ByteHoard principal.";

$bhlang['module:groups'] = "Grupos";
$bhlang['moduledesc:groups'] = "Crie e gerencie grupos de usu�rios.";

$bhlang['text:no_groups'] = "N�o h� atualmente nenhum grupo.";

$bhlang['notice:user_added_to_group'] = "Uu�rio '#USERNAME#' foi adicionado ao grupo '#GROUP#'.";
$bhlang['notice:user_removed_from_group'] = "Usu�rio '#USERNAME#' foiremovido do grupo '#GROUP#'.";
$bhlang['error:user_is_in_group'] = "Usu�rio '#USERNAME#' j� est�nogrupo '#GROUP#'.";
$bhlang['error:user_does_not_exist'] = "Usu�rio '#USERNAME# n�o existe!";

$bhlang['title:file_download'] = "Arquivo de Download";
$bhlang['label:filesize'] = "Tamanho do Arquivo: ";
$bhlang['label:filename'] = "Nomedo Arquivo: ";
$bhlang['label:from'] = "De: ";
$bhlang['label:md5'] = "MD5 Hash: ";
$bhlang['button:download_file'] = "Download #FILENAME#";
$bhlang['error:md5_file_too_large'] = "(arquivo muito grande; MD5 n�o calculado)";

$bhlang['explain:filelink_download'] = "O download do arquivo iniciar� em alguns segundos. se n�o iniciar, clique na conex�o abaixo. Se o seu browser abre o documento ao invez de salv�-lo, voce pode clicar com obot�o direito do mouseyou noarquivo e selecionar 'Salvar Como...' ou 'Salvar Alvo Como...' para salvar o arquivo para seu computador.";

$bhlang['error:quota_exceeded'] = "Fazendo isto voce excederia a sua quota em #QUOTA#.";

$bhlang['label:settings_lang'] = "Arquivo de Idioma: ";
$bhlang['explain:settings_lang'] = "Arquivo de idioma a ser usado.";

$bhlang['label:quota'] = "Quota: ";

$bhlang['label:_mb'] = "MB";

$bhlang['explain:edit_quota'] = "(Note: 0 ou caixa em branco significa espa�o ilimitado)";
$bhlang['error:quota_not_a_number'] = "A quota que voc� introduziu n�o � um n�mero v�lido!";

$bhlang['title:quota'] = "Quota";
$bhlang['explain:you_have_used_some_quota'] = "Voce j� usou #QUOTAUSED# de #QUOTA# de sua quota.";

$bhlang['title:types_administration'] = "Tipos de Usu�rios/Administra��o de Quota";
$bhlang['error:missed_something'] = "Voc� n�o deu um valor de um campo requerido.";
$bhlang['explain:types_administration'] = "Use esta tela para criar e eliminar tipos de usu�rio (quotas/plano de pre�os).";
$bhlang['column:type_name'] = "Tipo Nome";
$bhlang['column:type_size'] = "Tipo Quota";

$bhlang['module:types'] = "Usu�rio Tipeo";
$bhlang['moduledesc:types'] = "Editar tipos de usu�rio";

$bhlang['column:edit_quota'] = "Editar Quota (em MB)";
$bhlang['column:delete'] = "Deletar";

$bhlang['notice:type_updated'] = "Tipo Criado/Atualizado.";

