<?php

/*
 * ByteHoard 2.1
 * Copyright (c) Andrew Godwin & contributors 2004
 *
 *   satellite.php
 *   $Id$
 *
 */
 
define("IN_BH", 1);

# Satellite Server file

# Satellite is the nickname for the protocol I'll be using for remote connections.
# It's designed for ByteHoard, and although there is also a WebDAV server ticking over
# this can be implemented in python, java, etc to get a real client.
# It's a relatively simple protocol, entirely HTTP-based of course... there WILL be a reference at some point.

# First implementation is in Python and is in python/satellite.

# Also, I may spell satellite wrong a few times.... that's just me.
# And if there's any bugs, it's likely to be a consequence of switching from Python to PHP and back in a short amount of time.

error_reporting(0);

# Get includes
require "includes/configfunc.inc.php";				# Config functions
bh_checkconfig();						# Check the config exists, or don't bother with the rest.
require "config.inc.php";					# Load config file
require "includes/db/".$dbconfig['dbmod'];			# Database functions (TODO: make user-selectable)
bh_loadconfig();						# Load configuration
require "includes/auth/".$bhconfig['authmodule'];		# Authentication functions 
require "langs/".$bhconfig['lang'].".lang.php";					# Language File 
require "includes/filesystem/".$bhconfig['filesystemmodule']."/filesystem.inc.php";	# Filesystem functions 
require "includes/filesystem/".$bhconfig['filesystemmodule']."/mimetype.inc.php";	# Mimetype functions
require "includes/filesystem/".$bhconfig['filesystemmodule']."/thumbnails.inc.php";	# Thumbnail functions 
require "layouts/".$bhconfig['layout']."/main.inc.php";		# Layout file 
require "includes/log.inc.php";					# Logging functions
require "includes/users.inc.php";				# User functions
require "includes/modules.inc.php";				# Module functions
require "includes/detect.inc.php";				# Detection functions
require "includes/bandwidth.inc.php";				# Bandwidth functions

# Web absolute path. Author: pulstar at ig dot com dot br
# From www.php.net/realpath
function htmlpath($relative_path) {
	$realpath=realpath($relative_path);
	$htmlpath=str_replace($_SERVER['DOCUMENT_ROOT'],'',$realpath);
	return $htmlpath;
}

## Error function. Will spit out error then die
function bh_satellite_error($errorcode, $errormsg) {
	header("X-ByteHoard-Satellite-Error: ".$errorcode);
	echo "Error ".$errorcode.": ".$errormsg;
	die();
}

# Define some versions
$bhsatellite['protocolversion'] = "1";
$bhsatellite['sitename'] = $bhconfig['sitename'];
$bhsatellite['sitedesc'] = $bhconfig['sitedesc'];
$bhsatellite['chunksize'] = (1024 * 1024);
$bhsatellite['webaddress'] = "http".$httpsuff."://".$_SERVER['HTTP_HOST']."/".htmlpath(".");

# Get the command we're responding to. This must be a GET var.
$command = $_GET['command'];

# Get the credentials of the user
if (bh_authenticate($_POST['username'], $_POST['password']) == false) {
	#if ($command = info) {		# We let them get info if they're not registered, but that's it.
	#	$firsttimeround = 1;
	#	foreach ($bhsatellite as $paramname=>$paramvalue) {
	#		if ($firsttimeround == 0) { echo "\n"; } else { $firsttimeround = 0; }
	#		echo $paramname."=".$paramvalue;
	#	}
	#	die();
	#} else {
		bh_satellite_error("BHS_INVLOGIN", "Those login details are incorrect.");
	#}
} else {
	$bhcurrent['username'] = $_POST['username'];
	$bhsession['username'] = $_POST['username'];
	$bhcurrent['userobj'] = new bhuser($_POST['username']);
	$bhcurrent['usertype'] = $bhcurrent['userobj']->type;
}

switch ($command) {
	case "info":
		$firsttimeround = 1;
		foreach ($bhsatellite as $paramname=>$paramvalue) {
			if ($firsttimeround == 0) { echo "\n"; } else { $firsttimeround = 0; }
			echo $paramname."=".$paramvalue;
		}
		break;
	case "dirlist":
		$filepath = bh_fpclean($_GET['filepath']);
		if (bh_file_exists($filepath) == false) { bh_satellite_error("BHS_INVPATH", "That filepath does not exist"); }
		$fileobj = new bhfile($filepath);
		
		$firsttimeround = 1;
		if ($fileobj->is_dir() == TRUE) {
			$filelist = $fileobj->loadfile();
			foreach ($filelist as $afile) {
				if ($firsttimeround == 0) { echo "\n"; } else { $firsttimeround = 0; }
				unset($afileobj);
				$afileobj = new bhfile(bh_fpclean($filepath."/".$afile['filename']));
				echo "filename=".$afile['filename']."|||mimetype=".$afileobj->mimetype();
			}
		} else {
			bh_satellite_error("BHS_INVPATHTYPE", "That filepath is the wrong type for this command");
		}
		break;
	case "fileinfo":
		$filepath = bh_fpclean($_GET['filepath']);
		if (bh_file_exists($filepath) == false) { bh_satellite_error("BHS_INVPATH", "That filepath does not exist"); }
		$fileobj = new bhfile($filepath);
		echo "filename=".bh_get_filename($filepath)."\n";
		echo "filepath=".$filepath."\n";
		$firsttimeround = 1;
		foreach ($fileobj->fileinfo as $paramname=>$paramvalue) {
			if ($firsttimeround == 0) { echo "\n"; } else { $firsttimeround = 0; }
			echo $paramname."=".$paramvalue;
		}
		echo "\nmimetype=".$fileobj->mimetype();
		break;
	case "download":
		$filepath = bh_fpclean($_GET['filepath']);
		if (bh_file_exists($filepath) == false) { bh_satellite_error("BHS_INVPATH", "That filepath does not exist"); }
		$fileobj = new bhfile($filepath);
		if ($fileobj->is_dir() == TRUE) { bh_satellite_error("BHS_INVPATHTYPE", "That filepath is the wrong type for this command"); }
		$fileobj->readfile();
		break;
	default:
		bh_satellite_error("BHS_NOCMD", "You must provide a commmand.");
		break;
}